import constants from '../Constants/voucher';
import requestActions from './request';
import VoucherService from '../Services/voucher';

const setListVoucher = (list) => ({ type: constants.LIST_VOUCHER, list});
const setRetrieveVoucher = (detail) => ({ type: constants.RETRIEVE_VOUCHER, detail});
const setCreateBulkVoucherList = (response) => ({ type: constants.CREATE_BULK_VOUCHER, response });

class VoucherActions {

  static retrieve(voucher_id) {
    return (dispatch) => {
      let method = VoucherService.retrieve(voucher_id);
      requestActions.request(method, dispatch)
      .then((response) => {
        response.detail = response.data;
        dispatch(setRetrieveVoucher(response.detail))
      })
      .catch(err => {})
    }
  }

  static bulkCreate(body) {
    return (dispatch) => {
      let method = VoucherService.bulkCreate(body);
      requestActions.request(method, dispatch)
      .then((response) => {
        dispatch(setCreateBulkVoucherList(response))
      })
      .catch(err => {})
    }
  }

  static update(id, body) {
    return (dispatch) => {
      let method = VoucherService.update(id, body);
      requestActions.request(method, dispatch)
      .then((response) => {
        dispatch(setRetrieveVoucher(response.detail))
      })
      .catch(err => {})
    }
  }

  static delete(body) {
    return (dispatch) => {
      let method = VoucherService.delete(body);
      requestActions.request(method, dispatch)
      .then(response => {})
      .catch(err => {})
    }
  }

}

export default VoucherActions;
