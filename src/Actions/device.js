import constants from '../Constants/device';
import service from '../Services/device';
import requestActions from './request';

const listDevice = (list) => ({ type: constants.LIST_DEVICE, list })
const retrieveDevice = (detail) => ({ type: constants.RETRIEVE_DEVICE, detail })
const clearListDevice = () => ({ type: constants.CLEAR_LIST_DEVICE })

const actions = {}

actions.list = (user_id) => {
  return (dispatch) => {
    dispatch(clearListDevice())
    requestActions.request(service.list(user_id), dispatch)
    .then((response) => {
      dispatch(listDevice(response.data));
    })
    .catch((err) => {})
  }
}


actions.clearList = () => {
  return (dispatch) => {
    dispatch(clearListDevice())
  }
}


export default actions;