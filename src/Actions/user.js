import constants from '../Constants/user';
import service from '../Services/user';
import requestActions from './request';

const listUser = (list) => ({ type: constants.LIST_USER, list });
const retrieveUser = (detail) => ({ type: constants.RETRIEVE_USER, detail });

const actions = {};

actions.list = (queryObject={}) => {
  return (dispatch) => {
    let method = service.list(queryObject);
    requestActions.request(method, dispatch)
    .then((response) => {
      dispatch(listUser(response.data));
    })
    .catch((err) => {});
  };
};

actions.retrieve = (userId, userOverride) => {
  return (dispatch) => {
    if (userOverride) {
      dispatch(retrieveUser(userOverride));
    } else {
      requestActions.request(service.retrieve(userId), dispatch)
      .then((response) => {
        dispatch(retrieveUser(response.data));
      })
      .catch((err) => {});
    }
  };
};

actions.sendForgotPassword = (email) => {
  return (dispatch) => {
    requestActions.request(service.forgotPass(email), dispatch)
    .then((response) => {
      console.log("RES: ", response);
    })
    .catch((err) => {});
  };
};

actions.resendVerification = (email) => {
  return (dispatch) => {
    requestActions.request(service.resendVerif(email), dispatch)
    .then((response) => {
      console.log("RES: ", response);
    })
    .catch((err) => {});
  };
};


export default actions;
