import constants from '../Constants/discount';
import service from '../Services/discount';
import requestActions from './request';
import formActions from './form';

const listDiscount = (list) => ({ type: constants.LIST_DISCOUNT, list })
const retrieveDiscount = (detail) => ({ type: constants.RETRIEVE_DISCOUNT, detail })
const bulkDiscountList = (response) => ({ type: constants.BULK_DISCOUNT_CREATE, response })

class DiscountActions {
  static list(queryObject={}) {
    return (dispatch) => {
      let method = service.list(queryObject) 
      requestActions.request(method, dispatch)
      .then((response) => {
        dispatch(listDiscount(response.data));
      })
      .catch((err) => {})
    }
  }

  static retrieve(id, userOverride) {
    return (dispatch) => {
      if (userOverride) {
        dispatch(retrieveDiscount(userOverride));
      } else {
        requestActions.request(service.retrieve(id), dispatch)
        .then((response) => {
          dispatch(retrieveDiscount(response.data));
        })
        .catch((err) => {})
      }
    }
  }

  static create(body) {
    return (dispatch) => {
      requestActions.request(service.create(body), dispatch)
      .then((response) => {
        dispatch(this.list());
        dispatch(formActions.toggleModal(false));
      })
      .catch((err) => {})
    }
  }

  static bulkCreate(body) {
    return (dispatch) => {
      requestActions.request(service.bulkCreate(body), dispatch)
      .then((response) => {
        dispatch(bulkDiscountList(response));
      })
      .catch((err) => {})
    }
  }

  static update(body) {
    return (dispatch) => {
      requestActions.request(service.update(body), dispatch)
      .then((response) => {
        dispatch(this.retrieve(body.id));
        dispatch(formActions.toggleModal(false));
      })
      .catch((err) => {})
    }
  }
}


export default DiscountActions;
