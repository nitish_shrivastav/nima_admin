import constants from '../Constants/mobileDevice';
import service from '../Services/mobileDevice';
import requestActions from './request';

const listMobileDevice = (list) => ({ type: constants.LIST_MOBILE_DEVICE, list })
const retrieveMobileDevice = (detail) => ({ type: constants.RETRIEVE_MOBILE_DEVICE, detail })

const actions = {}

actions.list = (user_id) => {
  return (dispatch) => {
    requestActions.request(service.list(user_id), dispatch)
    .then((response) => {
      dispatch(listMobileDevice(response.data));
    })
    .catch((err) => {})
  }
}


export default actions;