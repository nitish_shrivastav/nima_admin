import constants from '../Constants/form';

const toggleModalIsOpen = (modalIsOpen) => ({ type: constants.TOGGLE_FORM_MODAL, modalIsOpen })

const actions = {}

actions.toggleModal = (isOpen) => {
  return (dispatch) => {
    dispatch(toggleModalIsOpen(isOpen))
  }
}

export default actions;