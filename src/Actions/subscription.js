import constants from '../Constants/subscription';
import service from '../Services/subscription';
import formActions from './form';
import requestActions from './request';

const listSubscription = (list) => ({ type: constants.LIST_SUBSCRIPTION, list })
const retrieveSubscription = (detail) => ({ type: constants.RETRIEVE_SUBSCRIPTION, detail })
const clearListSubscription = () => ({ type: constants.CLEAR_LIST_SUBSCRIPTION })
const clearDetailSubscription = () => ({ type: constants.CLEAR_DETAIL_SUBSCRIPTION })
const clearAllSubscription = () => ({ type: constants.CLEAR_ALL_SUBSCRIPTION })
const subscriptionSelect = (selectedSubscription) => ({ type: constants.SELECT_SUBSCRIPTION, selectedSubscription })

const actions = {}

actions.list = (params) => {
  return (dispatch) => {
    dispatch(clearListSubscription())
    requestActions.request(service.list(params), dispatch)
    .then((response) => {
      dispatch(listSubscription(response.data));
    })
    .catch((err) => {})
  }
}

actions.retrieve = (user_id, selectedSubscription) => {
  return (dispatch) => {
    dispatch(clearDetailSubscription())
    requestActions.request(service.retrieve(user_id), dispatch)
    .then((response) => {
      dispatch(retrieveSubscription(response.data));
      if (selectedSubscription) {
        for (let subscription of response.data.Items) {
          if (selectedSubscription.id === subscription.id && selectedSubscription.order === subscription.order) {
            dispatch(subscriptionSelect(subscription))
            break;
          }
        }
      }
    })
    .catch((err) => {})
  }
}

actions.update = (body) => {
  return (dispatch) => {
    requestActions.request(service.update(body), dispatch)
    .then((response) => {
      dispatch(formActions.toggleModal(false))
      dispatch(actions.retrieve(body.id, body))
    })
    .catch((err) => {})
  }
}

actions.destroy = (body) => {
  return (dispatch) => {
    requestActions.request(service.destroy(body), dispatch)
    .then((response) => {
      dispatch(actions.retrieve(body.id))
    })
    .catch((err) => {})
  }
}

actions.selectSubscription = (subscription) => {
  return (dispatch) => {
    dispatch(subscriptionSelect(subscription))
  }
}

actions.clearList = () => {
  return (dispatch) => {
    dispatch(clearListSubscription())
  }
}

actions.clearDetail = () => {
  return (dispatch) => {
    dispatch(clearDetailSubscription())
  }
}

actions.clearAll = () => {
  return (dispatch) => {
    dispatch(clearAllSubscription())
  }
}


export default actions;