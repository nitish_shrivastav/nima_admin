import constants from '../Constants/message';

const clearMessages = () => ({ type: constants.CLEAR_MESSAGES })
const setError = (message) => ({ type: constants.SET_ERROR_MESSAGE, message })

const actions = {}

actions.clearAllMessages = () => {
  return (dispatch) => {
    dispatch(clearMessages())
  }
}

actions.setErrorMessage = (message) => {
  return (dispatch) => {
    dispatch(actions.clearAllMessages())
    dispatch(setError(message))
  }
}

export default actions;