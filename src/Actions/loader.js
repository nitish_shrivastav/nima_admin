import constants from '../Constants/loader';

const setLoading = (isLoading) => ({ type: constants.SET_IS_LOADING, isLoading })

const actions = {}

actions.setIsLoading = (isLoading) => {
  return (dispatch) => {
    dispatch(setLoading(isLoading))
  }
}

export default actions;