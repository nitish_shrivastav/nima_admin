import authConstants from '../Constants/auth';
import authService from '../Services/auth';
import amazonStorage from '../Storage/amazonStorage';
import requestActions from './request';

const authLogin = (user) => ({ type: authConstants.AUTH_LOGIN, user })
const authLogout = () => ({ type: authConstants.AUTH_LOGOUT })

const authActions = {}

authActions.login = (credentials) => {
  return (dispatch) => {
    requestActions.request(authService.login(credentials), dispatch)
    .then((response) => {
      amazonStorage.setAmazonUser(response.data);
      dispatch(authLogin(response.data));
    })
    .catch((err) => {})
  }
}

authActions.refreshAuth = (refreshObj) => {
  return (dispatch) => {
    requestActions.request(authService.refresh(refreshObj), dispatch)
    .then((response) => {
      let amazonUser = amazonStorage.getAmazonUser();
      amazonUser.tokens = response.data.tokens;
      amazonStorage.setAmazonUser(amazonUser);
      dispatch(authLogin(amazonUser));
    })
    .catch((err) => {})
  }
}

authActions.logout = () => {
  return (dispatch) => {
    dispatch(authLogout());
  }
}


export default authActions;