import _ from 'lodash';
import constants from '../Constants/review';
import service from '../Services/review';
import requestActions from './request';

const listReview = (list) => ({ type: constants.LIST_REVIEW, list })
const retrieveReview = (detail, selectedReading) => ({ type: constants.RETRIEVE_REVIEW, detail, selectedReading })
const setReviewReadings = (readingList) => ({ type: constants.SET_READING_LIST_REVIEW, readingList })
const clearListReview = () => ({ type: constants.CLEAR_LIST_REVIEW })
const clearDetailReview = () => ({ type: constants.CLEAR_DETAIL_REVIEW })
const clearAllReviews = () => ({ type: constants.CLEAR_ALL_REVIEW })

const actions = {}

const _concatReadingsFromReview = (reviews) => {
  let concattedReviews = [];
  reviews.forEach((review) => {
    review.readings.forEach((reading) => {
      reading["review_id"] = review.id;
    })
    concattedReviews = concattedReviews.concat(review.readings);
  })
  return concattedReviews;
}

actions.list = (user_id) => {
  return (dispatch) => {
    dispatch(clearListReview())
    requestActions.request(service.list(user_id), dispatch)
    .then((response) => {
      dispatch(setReviewReadings(_concatReadingsFromReview(response.data)));
      dispatch(listReview(response.data));
    })
    .catch((err) => {})
  }
}

actions.retrieve = (id, user_id, reading_id) => {
  return (dispatch) => {
    dispatch(clearDetailReview())
    requestActions.request(service.retrieve(id, user_id), dispatch)
    .then((response) => {
      let extractedRes = response.data[0] ? response.data[0] : null;
      let selectedReading = null;
      if (reading_id && extractedRes) {
        let readingIndex = _.findIndex(extractedRes.readings, { "id": reading_id });
        selectedReading = extractedRes.readings[readingIndex];
      }
      dispatch(retrieveReview(extractedRes, selectedReading));
    })
    .catch((err) => {})
  }
}

actions.clearList = () => {
  return (dispatch) => {
    dispatch(clearListReview())
  }
}

actions.clearDetail = () => {
  return (dispatch) => {
    dispatch(clearDetailReview())
  }
}

actions.clearAll = () => {
  return (dispatch) => {
    dispatch(clearAllReviews())
  }
}


export default actions;