import loaderActions from './loader'
import messageActions from './message'

const actions = {}

actions.request = (method, dispatch) => {
  dispatch(loaderActions.setIsLoading(1))
  return method.then((response) => {
    dispatch(loaderActions.setIsLoading(-1))
    return response;
  })
  .catch((err) => {
    console.log('err', err)
    dispatch(loaderActions.setIsLoading(-1))
    dispatch(messageActions.setErrorMessage(err.message))
    throw Error(err);
  })

}

export default actions;