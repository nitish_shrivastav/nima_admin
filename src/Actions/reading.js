import constants from '../Constants/reading';
import service from '../Services/reading';
import requestActions from './request';
import reviewActions from './review';

const listReading = (list) => ({ type: constants.LIST_READING, list })
const retrieveReading = (detail) => ({ type: constants.RETRIEVE_READING, detail })
const clearListReading = () => ({ type: constants.CLEAR_LIST_READING })
const clearDetailReading = () => ({ type: constants.CLEAR_DETAIL_READING })
const clearAllReading = () => ({ type: constants.CLEAR_ALL_READING })

const actions = {}

actions.list = (user_id) => {
  return (dispatch) => {
    dispatch(clearListReading())
    requestActions.request(service.list(user_id), dispatch)
    .then((response) => {
      dispatch(listReading(response.data));
    })
    .catch((err) => {})
  }
}

actions.retrieve = (id, user_id) => {
  return (dispatch) => {
    dispatch(clearDetailReading())
    requestActions.request(service.retrieve(id, user_id), dispatch)
    .then((response) => {
      let extractedRes = response.data[0] ? response.data[0] : null;
      dispatch(retrieveReading(extractedRes));
      if (extractedRes && extractedRes.review_id) {
        dispatch(reviewActions.retrieve(extractedRes.review_id, user_id, id));
      } else {
        dispatch(reviewActions.clearAll());
      }
    })
    .catch((err) => {})
  }
}

actions.clearList = () => {
  return (dispatch) => {
    dispatch(clearListReading())
  }
}

actions.clearDetail = () => {
  return (dispatch) => {
    dispatch(clearDetailReading())
  }
}

actions.clearAll = () => {
  return (dispatch) => {
    dispatch(clearAllReading())
  }
}


export default actions;