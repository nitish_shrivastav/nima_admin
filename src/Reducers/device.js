import constants from '../Constants/device';

const initialState = {
  list: [],
  detail: null
}

export default function device(state=initialState, action) {
  switch (action.type) {
    case constants.LIST_DEVICE:
      return {
        ...state,
        list: action.list
      };
    case constants.RETRIEVE_DEVICE:
      return {
        ...state,
        detail: action.detail
      };
    case constants.CLEAR_LIST_DEVICE:
      return {
        ...state,
        list: []
      };
    default:
      return state
  }
}