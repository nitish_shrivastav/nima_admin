import constants from '../Constants/auth';

const initialState = {
  user: null
}

export default function auth (state=initialState, action) {
  switch (action.type) {
    case constants.AUTH_LOGIN:
      return {
        ...state,
        user: action.user
      };
    case constants.AUTH_LOGOUT:
      return initialState;
    default:
      return state
  }
}