import constants from '../Constants/reading';

const initialState = {
  list: [],
  detail: null
}

export default function reading(state=initialState, action) {
  switch (action.type) {
    case constants.LIST_READING:
      return {
        ...state,
        list: action.list
      };
    case constants.RETRIEVE_READING:
      return {
        ...state,
        detail: action.detail
      };
    case constants.CLEAR_LIST_READING:
      return {
        ...state,
        list: []
      };
    case constants.CLEAR_DETAIL_READING:
      return {
        ...state,
        detail: null
      };
    case constants.CLEAR_ALL_READING:
      return initialState;
    default:
      return state
  }
}