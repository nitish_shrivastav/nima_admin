import constants from '../Constants/message';

const initialState = {
  errorMessage: null
}

export default function message(state=initialState, action) {
  switch (action.type) {
    case constants.SET_ERROR_MESSAGE:
      return {
        errorMessage: action.message
      };
    case constants.CLEAR_MESSAGES:
      return initialState;
    default:
      return state
  }
}