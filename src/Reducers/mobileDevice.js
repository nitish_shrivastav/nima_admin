import constants from '../Constants/mobileDevice';

const initialState = {
  list: [],
  detail: null
}

export default function mobileDevice(state=initialState, action) {
  switch (action.type) {
    case constants.LIST_MOBILE_DEVICE:
      return {
        ...state,
        list: action.list
      };
    case constants.RETRIEVE_MOBILE_DEVICE:
      return {
        ...state,
        detail: action.detail
      };
    default:
      return state
  }
}