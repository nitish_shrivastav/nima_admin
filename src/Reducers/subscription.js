import constants from '../Constants/subscription';

const initialState = {
  list: [],
  detail: null,
  selectedSubscription: null
}

export default function subscription(state=initialState, action) {
  switch (action.type) {
    case constants.LIST_SUBSCRIPTION:
      return {
        ...state,
        list: action.list
      };
    case constants.RETRIEVE_SUBSCRIPTION:
      return {
        ...state,
        detail: action.detail
      };
    case constants.SELECT_SUBSCRIPTION:
      return {
        ...state,
        selectedSubscription: action.selectedSubscription
      };
    case constants.CLEAR_LIST_SUBSCRIPTION:
      return {
        ...state,
        list: []
      };
    case constants.CLEAR_DETAIL_SUBSCRIPTION:
      return {
        ...state,
        detail: null,
        selectedSubscription: null
      };
    case constants.CLEAR_ALL_SUBSCRIPTION:
      return initialState;
    default:
      return state
  }
}