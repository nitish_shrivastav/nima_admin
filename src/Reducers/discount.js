import constants from '../Constants/discount';

const initialState = {
  list: [],
  bulkList: null,
  detail: null
}

export default function discount(state=initialState, action) {
  switch (action.type) {
    case constants.LIST_DISCOUNT:
      return {
        ...state,
        list: action.list
      };
    case constants.BULK_DISCOUNT_CREATE:
      return {
        ...state,
        bulkList: action.response
      };
    case constants.RETRIEVE_DISCOUNT:
      return {
        ...state,
        detail: action.detail
      };
    case constants.CLEAR_LIST_DISCOUNT:
      return {
        ...state,
        list: []
      };
    case constants.CLEAR_DETAIL_DISCOUNT:
      return {
        ...state,
        detail: null
      };
    case constants.CLEAR_ALL_DISCOUNT:
      return initialState;
    default:
      return state
  }
}
