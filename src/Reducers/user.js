import constants from '../Constants/user';

const initialState = {
  list: [],
  detail: null
}

export default function user(state=initialState, action) {
  switch (action.type) {
    case constants.LIST_USER:
      return {
        ...state,
        list: action.list
      };
    case constants.RETRIEVE_USER:
      return {
        ...state,
        detail: action.detail
      };
    case constants.CLEAR_LIST_USER:
      return {
        ...state,
        list: []
      };
    case constants.CLEAR_DETAIL_USER:
      return {
        ...state,
        detail: null
      };
    case constants.CLEAR_ALL_USER:
      return initialState;
    default:
      return state
  }
}