import constants from '../Constants/voucher';

const initialState = {
  list: [],
  bulkList: null,
  detail: null
}

export default function voucher(state=initialState, action) {
  switch (action.type) {
    case constants.LIST_VOUCHER:
      return {
        ...state,
        list: action.list
      }
    case constants.RETRIEVE_VOUCHER:
      return {
        ...state,
        detail: action.detail
      }
    case constants.CREATE_BULK_VOUCHER:
      return {
        ...state,
        bulkList: action.response
      }
    default:
      return state
  }
}
