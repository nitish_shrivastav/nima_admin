import constants from '../Constants/review';

const initialState = {
  list: [],
  detail: null,
  selectedReading: null,
  readingList: []
}

export default function review(state=initialState, action) {
  switch (action.type) {
    case constants.LIST_REVIEW:
      return {
        ...state,
        list: action.list
      };
    case constants.RETRIEVE_REVIEW:
      return {
        ...state,
        detail: action.detail,
        selectedReading: action.selectedReading
      };
    case constants.SET_READING_LIST_REVIEW:
      return {
        ...state,
        readingList: action.readingList
      };
    case constants.CLEAR_LIST_REVIEW:
      return {
        ...state,
        list: []
      };
    case constants.CLEAR_DETAIL_REVIEW:
      return {
        ...state,
        detail: null,
        selectedReading: null
      };
    case constants.CLEAR_ALL_REVIEW:
      return initialState;
    default:
      return state
  }
}