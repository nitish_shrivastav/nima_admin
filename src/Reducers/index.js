import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import auth from './auth';
import discount from './discount';
import device from './device';
import form from './form';
import loader from './loader';
import message from './message';
import mobileDevice from './mobileDevice';
import reading from './reading';
import review from './review';
import subscription from './subscription';
import user from './user';
import voucher from './voucher';

export default combineReducers({
  router: routerReducer,
  auth,
  discount,
  device,
  nmform: form,
  message,
  mobileDevice,
  loader,
  reading,
  review,
  subscription,
  user,
  form: formReducer,
  voucher
})
