import constants from '../Constants/loader';

const initialState = {
  isLoading: 0
}

export default function loader(state=initialState, action) {
  switch (action.type) {
    case constants.SET_IS_LOADING:
      let loadingCount = state.isLoading + action.isLoading;
      if (loadingCount < 0) loadingCount = 0;
      return {
        isLoading: loadingCount
      };
    default:
      return state
  }
}