import constants from '../Constants/form';

const initialState = {
  modalIsOpen: false
}

export default function form(state=initialState, action) {
  switch (action.type) {
    case constants.TOGGLE_FORM_MODAL:
      return {
        modalIsOpen: action.modalIsOpen
      };
    default:
      return state
  }
}