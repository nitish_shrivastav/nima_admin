export default {
  RETRIEVE_DISCOUNT: "RETRIEVE_DISCOUNT",
  LIST_DISCOUNT: "LIST_DISCOUNT",
  CREATE_DISCOUNT: "CREATE_DISCOUNT",
  UPDATE_DISCOUNT: "UPDATE_DISCOUNT",
  DESTROY_DISCOUNT: "DESTROY_DISCOUNT",
  CLEAR_DETAIL_DISCOUNT: "CLEAR_DETAIL_DISCOUNT",
  CLEAR_LIST_DISCOUNT: "CLEAR_LIST_DISCOUNT",
  CLEAR_ALL_DISCOUNT: "CLEAR_ALL_DISCOUNT",
  BULK_DISCOUNT_CREATE: "BULK_DISCOUNT_CREATE"
}
