const constants = {};

constants.LIST_DEVICE = "LIST_DEVICE";
constants.RETRIEVE_DEVICE = "RETRIEVE_DEVICE";
constants.CLEAR_LIST_DEVICE = "CLEAR_LIST_DEVICE";

export default constants;