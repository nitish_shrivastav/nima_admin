const constants = {};

constants.RETRIEVE_USER = "RETRIEVE_USER";
constants.LIST_USER = "LIST_USER";
constants.CLEAR_DETAIL_USER = "CLEAR_DETAIL_USER";
constants.CLEAR_LIST_USER = "CLEAR_LIST_USER";
constants.CLEAR_ALL_USER = "CLEAR_ALL_USER";

export default constants;