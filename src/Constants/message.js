const constants = {};

constants.CLEAR_MESSAGES = "CLEAR_MESSAGES";
constants.SET_ERROR_MESSAGE = "SET_ERROR_MESSAGE";

export default constants;