const constants = {};

constants.AUTH_LOGIN = "AUTH_LOGIN";
constants.AUTH_LOGOUT = "AUTH_LOGOUT";

export default constants;