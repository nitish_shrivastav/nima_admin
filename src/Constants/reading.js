const constants = {};

constants.RETRIEVE_READING = "RETRIEVE_READING";
constants.LIST_READING = "LIST_READING";
constants.CLEAR_DETAIL_READING = "CLEAR_DETAIL_READING";
constants.CLEAR_LIST_READING = "CLEAR_LIST_READING";
constants.CLEAR_ALL_READING = "CLEAR_ALL_READING";

export default constants;