export default {
  api_url: "https://api-prod.nimasensor.com",
  host: "api-prod.nimasensor.com",
  api_version: "/v2",
  service: "execute-api",
  region: "us-west-2",
  shopify_domain: "https://nima-dev.myshopify.com",
  groupOptions: [
    {
      value: "",
      label: ""
    },
    {
      value: "all",
      label: "All Customers"
    },
    {
      value: "517087428622",
      label: "Gluten Premium Membership"
    },
    {
      value: "7511685070911",
      label: "Peanut Premium Membership"
    },
    {
      value: "26993622531",
      label: "Gluten Capsule Pack - One 12-pack every other month"
    },
    {
      value: "32154760846",
      label: "Gluten Capsule Pack - One 12-pack per month"
    },
    {
      value: "32154760910",
      label: "Gluten Capsule Pack - Two 12-packs per month"
    },
    {
      value: "32154760974",
      label: "Gluten Capsule Pack - One time purchase"
    },
    {
      value: "7511902191679",
      label: "Peanut Capsule Pack - One 12-pack every other month"
    },
    {
      value: "7512042143807",
      label: "Peanut Capsule Pack - One 12-pack per month"
    },
    {
      value: "7512048599103",
      label: "Peanut Capsule Pack - Two 12-packs per month"
    },
    {
      value: "7512057217087",
      label: "Peanut Capsule Pack - One time purchase"
    },
    {
      value: "1038081753102",
      label: "Sensor Only - Gluten"
    },
    {
      value: "7512557944895",
      label: "Sensor Only - Peanut"
    },
    {
      value: "26993973571",
      label: "Gluten Starter Kit"
    },
    {
      value: "1037948321806",
      label: "Peanut Starter Kit"
    },
    {
      value: "13813852471359",
      label: "Peanut Holidy Bundle"
    },
    {
      value: "13813841920063",
      label: "Gluten Holidy Bundle"
    },
    {
      value: "13798412681279",
      label: "Gluten Capsule voucher"
    },
    {
      value: "13798435684415",
      label: "Peanut Capsule voucher"
    },
    {
      value: "14050656911423",
      label: "Gluten Mini Bundle"
    },
    {
      value: "14050805284927",
      label: "Peanut Mini Bundle"
    },
    {
      value: "14051012149311",
      label: "Gluten Mini Bundle VOUCHER"
    },
    {
      value: "14050824454207",
      label: "Peanut Mini Bundle VOUCHER"
    },
    {
      value: "13798412681279",
      label: "Gluten Capsule VOUCHER"
    },
    {
      value: "13798435684415",
      label: "Peanut Capsule VOUCHER"
    },
    {
      value: "14227545260095",
      label: "Gluten Starter Kit w/ VOUCHER SUB"
    },
    {
      value: "14314508255295",
      label: "Peanut Starter Kit w/ VOUCHER SUB"
    }
  ]
}
