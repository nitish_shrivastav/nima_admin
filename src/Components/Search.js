import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SearchIcon from '../SVG/SearchIcon';

class Search extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillMount() {
    this.setState({
      searchValue: ''
    })
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.onSearchSubmit(this.state.searchValue)
  }

  handleChange(e) {
    this.setState({
      searchValue: e.target.value
    })
  }

  render() {
    return (
      <form className="nm-layout" onSubmit={this.handleSubmit}>
        <div className="nm-layout">
          <div onClick={this.handleSubmit}>
            <SearchIcon />
          </div>
          <input
            className="nm-search"
            placeholder={this.props.placeholder}
            onChange={this.handleChange}
            value={this.state.searchValue}/>
        </div>
      </form>
    );
  }
}

Search.Proptypes = {
  onSearchSubmit: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired
}

export default Search;