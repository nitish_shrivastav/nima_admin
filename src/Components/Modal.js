import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Close from './SVG/Close';

class Modal extends Component {
  constructor() {
    super();
    this.handleModalClick = this.handleModalClick.bind(this);
  }

  stopModalClick(e) {
    e.stopPropagation()
  }

  handleModalClick() {
    if (this.props.onModalClick) {
      this.props.onModalClick()
    }
  }

  render() {
    return (
      <div>
        {
          this.props.showModal &&
          <div className="nm-modal nm-modal__background" onClick={this.handleModalClick}>
            <div className={`nm-card ${this.props.className}`} onClick={this.stopModalClick}>
            {
              this.props.onCloseClick &&
              <Close className="nm-card__close" onClick={this.props.onCloseClick}/>
            }
            {this.props.children}
            </div>
          </div>
        }
      </div>
    );
  }
}

Modal.Proptypes = {
  showModal: PropTypes.bool.isRequired,
  onCloseClick: PropTypes.func,
  onModalClick: PropTypes.func,
  className: PropTypes.string
}

export default Modal;