import React, { Component } from 'react';
import moment from 'moment';

class Input extends Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  
  componentWillMount() {
    this.setState({
      type: this.props.type ? this.props.type : "text",
      labelClass: "",
      hasFocused: false
    })
  }

  handleChange(e) {
    if (!this.props.field.isTime) {
      this.props.onChange(e.target.value, this.props.field.name);
    }
  }

  handleFocus() {
  }

  handleBlur(e) {
    if (this.props.onBlur) {
      this.props.onBlur(e)
    }
  }

  handleClick() {
    if (this.props.onClick) {
      this.props.onClick()
    }
  }

  render() {
    let wrapClass = this.props.className ? this.props.className : "";
    let inputClass = (this.props.showError && this.props.field.error) ? "nm-form__input-error" : "";
    let autocomplete = this.props.autocomplete ? this.props.autocomplete : "";
    let value = this.props.field.value;
    if (this.props.field.isTime) {
      value = moment(value).format('MMMM Do YYYY')
    }
    return (
      <div className={`nm-layout nm-form__input-wrap ${wrapClass}`} onClick={this.handleClick}>
        <label className="nm-form__label">
          {this.props.field.label}
        </label>
        <input
          type={this.state.type}
          name={this.props.field.name}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          className={`nm-form__input ${inputClass}`}
          onChange={this.handleChange}
          autoComplete={autocomplete}
          value={value}/>
        {
          this.props.onRemove &&
          <p className="nm-layout nm-link" onClick={this.props.onRemove}>
            Remove
          </p>
        }
      </div>
    );
  }
}

export default Input;