import React, { Component } from 'react';

class DropDown extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }
  
  componentWillMount() {
    this.setState({
      type: this.props.type ? this.props.type : "text",
      labelClass: "",
      hasFocused: false,
      isOpen: false,
      ulClass: "closed"
    })
  }

  handleClick(option) {
    this.props.onChange(option);
  }

  handleFocus() {
    this.setState({
      ulClass: ""
    })
  }

  handleBlur(e) {
    if (this.props.onBlur) {
      this.props.onBlur(e)
    }
    setTimeout(() => {
      this.setState({
        ulClass: "closed"
      })
    }, 100)
  }


  render() {
    let wrapClass = this.props.className ? this.props.className : "";
    let loaderClass = this.props.isLoading ? "" : "nm-loader__hidden";
    return (
      <div className={`nm-layout nm-form__input-wrap ${wrapClass}`}>
        <label className="nm-form__label">
          {this.props.field.label}
        </label>
        <input
          name={this.props.field.name}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          className="nm-form__input nm-form__input--shadow"
          onChange={this.handleChange}
          readOnly
          value={this.props.field.text}/>
        <div className="nm-loader__field"> 
          <div className={`nm-loader nm-loader__blue ${loaderClass}`} style={{fontSize: 15}}></div>
        </div>
        <span className="nm-form__drop-icon">▾</span>
        <ul className={`nm-form__ul ${this.state.ulClass}`} style={{top: 22}}>
          {
            this.props.options.map((option) => {
              return (
                <li
                  style={{paddingLeft: 12}}
                  className="nm-form__li"
                  onMouseDown={() => {this.handleClick(option)}}>
                  {this.props.textExtractor(option)}
                </li>
              )
            })
          }
        </ul>
      </div>
    );
  }
}

export default DropDown;