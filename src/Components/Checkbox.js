import React, { Component } from 'react';
import Check from './SVG/Check';

class Checkbox extends Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
  }
  
  handleChange(e) {
    this.props.onChange(!this.props.field.value, this.props.field.name)
  }

  render() {
    return (
      <div className="nm-layout">
        <div className="nm-form__checkbox-wrap">
          <input
            type="checkbox"
            id={`${this.props.field.name}_id`}
            name={this.props.field.name}
            className="nm-form__checkbox"
            onChange={this.handleChange}
            checked={this.props.field.value}/>
          <div className="nm-form__checkbox-check">
            <Check /> 
          </div>
        </div>
        <label htmlFor={`${this.props.field.name}_id`} className="nm-form__checkbox-label">
          {this.props.field.label}
        </label>
      </div>
    );
  }
}

export default Checkbox;