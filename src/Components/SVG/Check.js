import React, { Component } from 'react';

class Check extends Component {
  componentWillMount() {
    this.setState({
      anmiateClass: ""
    })
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        anmiateClass: "active"
      })
    }, 300)
  }

  render() {
    return (
      <div>
        {
          this.props.animate &&  
          <svg className={`nm-btn__check--wrap ${this.props.className}`} viewBox="0 0 24 24" width="96px" height="96px" style={this.props.style}>
            <path className={`nm-btn__check ${this.state.anmiateClass}`} d="M 5.705,11.295 10,15.585 18.295,7.29" ></path>
          </svg>
        }
        {
          !this.props.animate &&
          <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 448.8 448.8">
            <g id="check">
              <polygon points="142.8,323.85 35.7,216.75 0,252.45 142.8,395.25 448.8,89.25 413.1,53.55   " fill="#FFFFFF"/>
            </g>
          </svg>
        }
      </div>
    );
  }
}

export default Check;