import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Close extends Component {

  render() {
    return (
      <svg onClick={this.props.onClick} className={`nm-image__fit-svg ${this.props.className}`} x="0px" y="0px"width="357px" height="357px" viewBox="0 0 357 357">
      	<polygon points="357,35.7 321.3,0 178.5,142.8 35.7,0 0,35.7 142.8,178.5 0,321.3 35.7,357 178.5,214.2 321.3,357 357,321.3 
			    214.2,178.5 		"/>
      </svg>
    );
  }
}

Close.defaultProps = {
  className: ''
}

Close.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired
}

export default Close;