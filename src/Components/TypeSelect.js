import React from 'react';
import PropTypes from 'prop-types';

const TypeSelect = (props) => {
  return (
      <div className="nm-layout">
        {
          props.items.map((item, index) => {
            let selectedStyle = item.value === props.selected ? "selected" : "";
            return (
              <p
                key={`${props.name}-${index}`}
                onClick={() => { props.onTypeClick(item, index) }}
                className={`nm-search__type ${selectedStyle}`}>
                {item.display}
              </p>
            )
          })
        }
      </div>
  );
}

TypeSelect.propTypes = {
  onTypeClick: PropTypes.func.isRequired,
  items: PropTypes.arrayOf(PropTypes.shape({
    display: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired
  })).isRequired,
  name: PropTypes.string.isRequired,
  selected: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired
}

export default TypeSelect;
