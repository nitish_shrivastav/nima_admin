import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class Table extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }
  
  componentWillMount() {
    this.setState({
      rowClass: this.props.onRowClick ? "nm-table__row--hover" : "",
      selectedRow: null
    })
  }

  handleClick(row, index) {
    if (this.props.onRowClick) {
      this.props.onRowClick(row)
      this.setState({
        selectedRow: index
      })
    }
  }

  render() {
    const { 
      columns,
      rows,
      setDetailRoute,
      isExternalLink,
      alternativeColumnHeaderForDetails
    } = this.props;
    const { selectedRow, rowClass } = this.state;
    return (
      <div className="nm-layout">
        <table className="nm-table nm-layout">
          <thead className="nm-table__head">
            <tr className="nm-table__head">
              {
                columns.map((column, index) => {
                  return (
                    <th key={index} className="nm-table__td">{column.name}</th>
                  )
                })
              }
              {
                setDetailRoute &&
                <th className="nm-table__td">
                  {alternativeColumnHeaderForDetails}
                </th>
              }
            </tr>
          </thead>
          <tbody>
            {
              rows.map((column, index) => {
                  let selectedClass = index === selectedRow ? "nm-table__row--selected" : "";
                  return (
                    <tr
                      key={index}
                      className={`nm-table__row ${rowClass} ${selectedClass}`}
                      onClick={() => this.handleClick(column, index)}>
                      {
                        columns.map((columnHead, i) => {
                          let cellData = column[columnHead.field];
                          if (columnHead.prepData) {
                            cellData = columnHead.prepData(column);
                          }
                          return (
                            <td key={`table-${i}`} className="nm-table__td">
                              { cellData }
                            </td>
                          )
                        })
                      }
                      {
                        setDetailRoute && !isExternalLink &&
                        <td className="nm-table__td">
                          <Link to={setDetailRoute(column)} className="nm-link">
                            {alternativeColumnHeaderForDetails} &#8594;
                          </Link>
                        </td>
                      }
                      {
                        setDetailRoute && isExternalLink &&
                        <td className="nm-table__td">
                          <a href={setDetailRoute(column)} className="nm-link" target="_blank">
                            {alternativeColumnHeaderForDetails} &#8594;
                          </a>
                        </td>
                      }
                    </tr>
                  )
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

Table.defaultProps = {
  alternativeColumnHeaderForDetails: "DETAILS"
}

//Table.Proptypes = {
//	 columns: PropTypes.array.isRequired,
//	 rows: PropTypes.array.isRequired,
//   setDetailRoute: PropTypes.func,
//   rowPreCheck: PropTypes.func
//}

export default Table;
