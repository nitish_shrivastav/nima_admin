import React, { Component } from 'react';
import PropTypes from 'prop-types';

let Button = (props) => {
  const { disabled, className, center } = props;
  let disabledClass = disabled ? "nm-btn__disabled" : "nm-btn__hover";
  let centerClass = center ? "nm-btn__center" : "";
  return (
    <div
      onClick={props.onClick}
      style={props.style}
      className={`nm-btn ${className} ${centerClass} ${disabledClass}`}>
      { props.text }
      { props.children }
    </div>
  );
}

Button.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string.isRequired,
  className: PropTypes.string,
  center: PropTypes.bool,
  disabled: PropTypes.bool,
  style: PropTypes.object
}

Button.defaultProps = {
  style: {},
  className: "",
  disabled: false
}

export default Button;
