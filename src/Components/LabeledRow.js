import React, { Component } from 'react';
import PropTypes from 'prop-types';


class LabeledRow extends Component {
  render() {
    let col2Text = this.props.column2Text ? this.props.column2Text : "xxxxxx";
    return (
      <div className="nm-layout">
        <div className="nm-layout nm-layout__column">
          <h5 className="nm-layout nm-basic__h5">
            { this.props.column1Label }
          </h5>
          <p className="nm-layout">
            { this.props.column1Text }
          </p>
        </div>
        <div className="nm-layout nm-layout__column">
          {
            this.props.column2Label &&
            <div>
              <h5 className="nm-layout nm-basic__h5">
                { this.props.column2Label }
              </h5>
              {
                this.props.onColumn2Click &&
                <p className="nm-layout nm-link" onClick={this.props.onColumn2Click}>
                  { col2Text }
                </p>
              }
              {
                !this.props.onColumn2Click &&
                <p className="nm-layout">
                  { col2Text }
                </p>
              }
            </div>
          }
        </div>
        <div className="nm-spacer-12"/>
      </div>
    );
  }
}

LabeledRow.Proptypes = {
	 column1Label: PropTypes.string.isRequired,
	 column1Text: PropTypes.string,
	 column2Label: PropTypes.string,
	 column2Text: PropTypes.string
}

export default LabeledRow;
