import React, { Component } from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import InputError from '../InputError';
import Input from '../Input';
import 'react-datepicker/dist/react-datepicker.css';

let DateField = (props) => {
  const { input, label, meta } = props;
  return (
    <div className="nm-layout">
      <DatePicker
        selected={moment(input.value)}
        onChange={(date) => { props.onDateChange(new Date(moment(date).startOf('day')).getTime()) }}
        dateFormat="LLL"
        customInput={
          <Input
            field={{
              label: props.label,
              value: moment(input.value).format("MM/DD/YY")
            }}
            onChange={() => {}}/>
          } />
      <InputError
        show={meta.touched && meta.error}
        error={meta.error}/>
    </div>
  );
}

export default DateField;
