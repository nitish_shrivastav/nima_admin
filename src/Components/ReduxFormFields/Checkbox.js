import React, { Component } from 'react';
import Check from '../SVG/Check';

let Checkbox = (props) => {
  const { input, label } = props;
  return (
    <div className="nm-layout">
      <div className="nm-form__checkbox-wrap">
        <input
          {...input}
          type="checkbox"
          className="nm-form__checkbox"
          checked={ input.value }
        />
        <div className="nm-form__checkbox-check">
          <Check /> 
        </div>
      </div>
      <label htmlFor={props.input.for} className="nm-form__checkbox-label">
        {label}
      </label>
    </div>
  );
}

export default Checkbox;
