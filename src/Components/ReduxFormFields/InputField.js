import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputError from '../InputError';

class InputField extends Component {
  constructor() {
    super();
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  handleKeyPress(e) {
    if (this.props.onKeyDown) {
      this.props.onKeyDown(e)
    }
  }

  render() {
    const { input, label, meta } = this.props;
    let inputClass = (meta.touched && meta.error) ? "nm-form__input-error" : "";
    return (
      <div className={`nm-layout nm-form__input-wrap ${this.props.className}`}>
        <label className="nm-form__label">
          {label}
        </label>
        <input
          {...input}
          onKeyDown={this.handleKeyPress}
          className={`nm-form__input ${inputClass}`}/>
        { this.props.children }
        <InputError
          show={meta.touched && meta.error}
          error={meta.error}/>
      </div>
    );
  }
}

InputField.defaultProps = {
  className: ""
}

InputField.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
  info: PropTypes.string,
  children: PropTypes.element
}

export default InputField;
