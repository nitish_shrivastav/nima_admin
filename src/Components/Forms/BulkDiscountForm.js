import { getPortPromise } from "portfinder";

import React, { Component } from 'react';
import { connect } from "react-redux";
import { Field, submit, reduxForm, change } from 'redux-form';
import moment from 'moment';
import config from '../../config'
import DateField from '../ReduxFormFields/DateField';
import Input from '../Input';
import Checkbox from '../ReduxFormFields/Checkbox';
import DropDown from '../DropDown';
import { notEmptyValidation, numberValidation,
         dateValidation, positiveNumberValidation } from '../../Helpers/reduxFormValidation';
import InputField from '../ReduxFormFields/InputField';
import Button from '../Button';

import 'react-datepicker/dist/react-datepicker.css';


const typeOptions = [
  {
    value: "fixed",
    text: "Fixed"
  },
  {
    value: "percent",
    text: "Percent"
  }
]

let BulkDiscountForm = (props) => {
  return (
    <div className="nm-layout">
      <h2>Bulk discount create</h2>
      <div className="nm-spacer-12"></div>
      <div className="nm-spacer-12"></div>
      <form className="nm-layout" onSubmit={props.onSubmit}>
        <Field
          name="prefix"
          label="Discount Prefix"
          validate={[notEmptyValidation]}
          component={InputField}/>
        <Field
          name="quantity"
          validate={[notEmptyValidation, positiveNumberValidation]}
          label="How many discounts?"
          component={InputField}/>
        <Field
          name="amount"
          label="Amount"
          validate={[notEmptyValidation, positiveNumberValidation]}
          component={InputField}/>
        <label className="nm-form__label">
          Type
        </label>
        <Field
          name="type_"
          label="Type"
          component="select">
          {
            typeOptions.map((option) => {
              return (
                <option value={option.value}>{option.text}</option>
              )
            })
          }
        </Field>
        <label className="nm-form__label">
          Products
        </label>
        {
          config.groupOptions.map((option) => {
            return (
              <Field
                label={option.label}
                name={`group.${option.value}`}
                component={Checkbox}/>
            )
          })
        }
        <div className="nm-spacer-12"></div>
        <Field
          name="started_at"
          label="Start date"
          validate={[dateValidation]}
          onDateChange={(value) => {props.dispatch(change('bulkDiscountCreate', 'started_at', value))}}
          component={DateField}/>
        <Field
          name="ended_at"
          label="End date"
          validate={[dateValidation]}
          onDateChange={(value) => {props.dispatch(change('bulkDiscountCreate', 'ended_at', value))}}
          component={DateField}/>
        <Field
          name="rules.min_amount"
          validate={[positiveNumberValidation]}
          label="Minimum Purchase Amount Required (required)"
          component={InputField}/>
        <Field
          name="rules.user_apply_limit"
          validate={[notEmptyValidation, positiveNumberValidation]}
          label="User apply limit"
          component={InputField}/>
        <Field
          name="rules.total_apply_limit"
          label="Total apply limit"
          component={InputField}/>
        <div className="nm-spacer-12"></div>
        <div className="nm-clear"></div>
        <Button
          text="Submit"
          onClick={() => props.dispatch(submit('bulkDiscountCreate'))}
          center={true}
          style={{maxWidth: 300}}/>
        <div className="nm-spacer-48"></div>
        <div className="nm-spacer-48"></div>
      </form>
    </div>
  )
}

BulkDiscountForm = reduxForm({
  form: 'bulkDiscountCreate'
})(BulkDiscountForm);

export default connect(
  (state) => ({
    initialValues: {
      type_: "fixed",
      started_at: new Date(moment().startOf('day')).getTime(),
      ended_at: new Date(moment().add(6, 'months').endOf('day')).getTime(),
      rules: {
        min_amount: 0,
        user_apply_limit: 1,
        total_apply_limit: 1
      }
    }
  })
)(BulkDiscountForm);

