import React from 'react';
import { connect } from 'react-redux';
import { Field, FieldArray, submit, reduxForm, change } from 'redux-form';
import moment from 'moment';

import Button from '../Button';
import Checkbox from '../ReduxFormFields/Checkbox';
import config from '../../config';
import DateField from '../ReduxFormFields/DateField';
import InputField from '../ReduxFormFields/InputField';
import { notEmptyValidation, numberValidation,
  dateValidation, positiveNumberValidation } from '../../Helpers/reduxFormValidation';

import 'react-datepicker/dist/react-datepicker.css';

const renderField = ({ input, label, type, meta: { touched, error } }) => {
  return (
    <div>
      <label>{label}</label>
      <div>
        <input {...input} type={type} placeholder={label} />
        {touched && error && <span>{error}</span>}
      </div>
    </div>
  )
}

const renderMembers = ({ fields, meta: { error, submitFailed } }) => {
  if (fields.length <= 0) fields.push({ quantity: 1 });
  return (
    <div>
      {
        submitFailed &&
        <div style={{ color: 'red' }}>Product name cannot be empty</div>
      }
      {
        fields.map((member, index) => (
          <div>
            <label className="nm-form__label" style={{ float: "none" }}>Product Name</label>
            <Field
              name={`${member}.variant_id`}
              type="text"
              component="select"
              validate={[notEmptyValidation]}
            >
              {
                config.groupOptions.map((option) => (
                  <option value={option.value}>{option.label}</option>
                ))
              }
            </Field>
            <label className="nm-form__label" style={{ float: "none" }}>Quantity</label>
            <Field
              name={`${member}.quantity`}
              component="select"
            >
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
            </Field>
            <button type="button" style={{ marginLeft: '20px', color: 'red' }} onClick={() => fields.remove(index)}>
              X
            </button>
            <div className="nm-spacer-12"></div>
          </div>
        ))
      }
      <div className="nm-clear"></div>
      <div className="nm-spacer-12"></div>
      <Button
        text="Add Product"
        center={true}
        onClick={() => fields.push({ quantity: 1 })}
        style={{ maxWidth: '150px', backgroundColor: '#D3D3D3' }}
      />
      <div className="nm-clear"></div>
    </div>
  )
}

let BulkVoucherForm = (props) => {
  return (
    <div className="nm-layout">
      <h2>Bulk voucher create</h2>
      <div className="nm-spacer-12"></div>
      <div className="nm-spacer-12"></div>
      <form className="nm-layout" onSubmit={props.onSubmit}>
        <Field
          name="voucher_quantity"
          label="How many discounts?"
          validate={[notEmptyValidation, positiveNumberValidation]}
          component={InputField}
        />
        <label className="nm-form__label">
          Select Products
        </label>
        <div className="nm-spacer-12"></div>
        <FieldArray
          name="line_items"
          type="text"
          component={renderMembers}
          label="products label"
        />
        <div className="nm-spacer-12"></div>
        <Field
          name="start_date"
          label="Start date"
          validate={[dateValidation]}
          onDateChange={(value) => {props.dispatch(change('bulkVoucherCreate', 'start_date', value))}}
          component={DateField}
        />
        <Field
          name="end_date"
          label="End date"
          validate={[dateValidation]}
          onDateChange={(value) => {props.dispatch(change('bulkVoucherCreate', 'end_date', value))}}
          component={DateField}
        />
        <div className="nm-spacer-12"></div>
        <div className="nm-clear"></div>
        <Button
          text="Submit"
          onClick={() => props.dispatch(submit('bulkVoucherCreate'))}
          center={true}
          style={{maxWidth: 300}}
        />
      </form>
      <div className="nm-spacer-48"></div>
      <div className="nm-spacer-48"></div>
    </div>
  )
}

BulkVoucherForm = reduxForm({
  form: 'bulkVoucherCreate'
})(BulkVoucherForm);

export default connect(
  (state) => ({
    initialValues: {
      start_date: new Date(moment().utc().startOf('day')).getTime(),
      end_date: new Date(moment().utc().add(6, 'months').endOf('day')).getTime()
    }
  })
)(BulkVoucherForm);
