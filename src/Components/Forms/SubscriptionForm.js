import React, { Component } from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import config from '../../config';
import Input from '../Input';
import Add from '../SVG/Add';
import Checkbox from '../Checkbox';
import timeHelper from '../../Helpers/timeHelper';
import DropDown from '../DropDown';

const formFields = {
  id: {
    value: '',
    label: "Customer Id",
    readonly: true
  },
  order_id: {
    value: '',
    label: "Order",
    readonly: true
  },
  skips: {
    type: 'array',
    label: 'Next 10 Skips',
    values: []
  },
  discounts: {
    type: 'array',
    label: "Discount Code",
    values: []
  },
  constant_price: {
    value: '',
    label: "Price Override",
    name: "constant_price"
  },
  quantity: {
    value: '',
    name: 'quantity',
    label: 'send',
    text: ''
  },
  shipping_interval_frequency: {
    value: '',
    name: 'shipping_interval_frequency',
    label: 'every',
    text: ''
  },
  next_ship_date: {
    value: moment().startOf('day'),
    name: 'next_ship_date',
    label: 'Next Ship Date',
    isTime: true
  }
}

const initialState = {
  error: undefined
}

const typeOptions = {
  quantity: [
    {
      value: '1',
      text: '1'
    },
    {
      value: '2',
      text: '2'
    },
    {
      value: '3',
      text: '3'
    },
    {
      value: '4',
      text: '4'
    },
    {
      value: '5',
      text: '5'
    },
    {
      value: '6',
      text: '6'
    }
  ],
  shipping_interval_frequency: [
    {
      value: '1',
      text: '1 month',
    },
    {
      value: '2',
      text: '2 months',
    },
    {
      value: '3',
      text: '3 months',
    }
  ]
}

const hasInitialSkip = (skips, timestamp) => {
  for (let skip of skips) {
    if (moment(parseInt(skip.created_at)).isSame(timestamp, 'day')) {
      return true;
    }
  }
  return false;
}

const getNextShipDate = (last_sent, send_day, shipping_interval_frequency) => {
  shipping_interval_frequency = parseInt(shipping_interval_frequency);
  send_day = send_day.split('|')[1];
  let nextSendDay = moment(parseInt(last_sent)).date(send_day);
  while (nextSendDay.isAfter(moment(), 'day') === false) {
    nextSendDay.add(shipping_interval_frequency, 'months');
  }
  return nextSendDay;
}

const getInitialFormFields = (initialFields, form) => {
  let fields = JSON.parse(JSON.stringify(form));
  if (initialFields) {
    for (let field of Object.keys(fields)) {
      if (initialFields[field] && (!fields[field].type || fields[field].type !== 'array')) {
        fields[field].value = initialFields[field];
      }
    }
    if (initialFields.discounts) {
      fields.discounts.values = initialFields.discounts.map((discount) => {
        return {
          value: discount,
          label: "Discount Code"
        }
      })
    }
    let skips = [];
    for (let i = 0; i < 10; i++) {
      let dateDelta = (i + 1) * parseInt(initialFields.shipping_interval_frequency);
      let shipDate = moment(parseInt(initialFields.last_sent)).date(parseInt(initialFields.send_day.split('|')[1])).add(dateDelta, initialFields.shipping_interval_type);
      skips.push({
        name: `skips_${i}`,
        label: timeHelper.dayFormat(shipDate),
        value: hasInitialSkip(initialFields.skips, shipDate)
      })
    }
    fields.skips.values = skips;
    fields.quantity.text = initialFields.quantity;
    fields.quantity.value = initialFields.quantity;
    fields.shipping_interval_frequency.text = `${initialFields.shipping_interval_frequency} month${initialFields.shipping_interval_frequency > 1 ? 's' : ''}`;
    fields.shipping_interval_frequency.value = initialFields.shipping_interval_frequency;
    fields.next_ship_date.value = getNextShipDate(initialFields.last_sent, initialFields.send_day, initialFields.shipping_interval_frequency)
  }
  return fields;
}

class SubscriptionForm extends Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.addDiscountField = this.addDiscountField.bind(this);
    this.setArrayState = this.setArrayState.bind(this);
    this.removeArrayIndex = this.removeArrayIndex.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
  }

  componentWillMount() {
    let initialFields = getInitialFormFields(this.props.initialFields, formFields);
    this.setState(Object.assign(initialState, initialFields))
  }

  handleChange(val, name) {
    this.setState({
      [name]: {
        ...this.state[name],
        value: val
      }
    })
  }

  handleDropdownClick(option, field) {
    this.setState({
      [field]: {
        ...this.state[field],
        value: option.value,
        text: option.text
      }
    })
  }

  addDiscountField() {
    let newDiscount = {
      value: '',
      label: 'Discount Code',
      error: undefined,
      name: 'discounts'
    }
    this.setState({
      discounts: {
        ...this.state.discounts,
        values: this.state.discounts.values.concat(newDiscount)
      }
    });
  }

  setArrayState(index, newObj, field) {
    this.setState({
      [field]: {
        ...this.state[field],
        values: [
          ...this.state[field].values.slice(0, index),
          Object.assign({}, this.state[field].values[index], newObj),
          ...this.state[field].values.slice(index + 1)
        ]
      }
    })
  }

  handleSubmit(e) {
    if (e) {
      e.preventDefault();
    }
    const { initialFields } = this.props;
    let skips = [];
    for (let index in this.state.skips.values) {
      if (this.state.skips.values[index].value) {
        let dateDelta = (parseInt(index) + 1) * parseInt(initialFields.shipping_interval_frequency);
        let shipDate = moment(parseInt(initialFields.last_sent)).date(parseInt(initialFields.send_day.split('|')[1])).add(dateDelta, initialFields.shipping_interval_type);
        skips.push({
          created_at: new Date(shipDate).getTime()
        })
      }
    }
    let sendDate = this.state.next_ship_date.value.date() > 28 ? 28 : this.state.next_ship_date.value.date();
    let body = {
      id: this.props.initialFields.id,
      order: this.props.initialFields.order,
      constant_price: this.state.constant_price.value === "" ? null : this.state.constant_price.value,
      discounts: this.state.discounts.values.map((discount) => {
        return {
          id: discount.value
        }
      }),
      skips: skips,
      quantity: parseInt(this.state.quantity.value),
      shipping_interval_frequency: this.state.shipping_interval_frequency.value,
      send_day: `months|${sendDate}`,
      last_sent: new Date(moment(this.state.next_ship_date.value).startOf('day').subtract(this.state.shipping_interval_frequency.value, 'months')).getTime()
    }
    this.props.onSubmit(body)
    this.setState({
      error: undefined
    })
  }

  removeArrayIndex(index, field) {
    this.setState({
      [field]: {
        ...this.state[field],
        values: [
          ...this.state[field].values.slice(0, index),
          ...this.state[field].values.slice(index + 1)
        ]
      }
    })
  }

  _getDropdownText(field) {
    return field.text;
  }

  handleDateChange(date) {
    this.setState({
      next_ship_date: {
        ...this.state.next_ship_date,
        value: date
      }
    })
  }

  render() {
    return (
      <div>
        <h2>Subscription {this.props.methodText}</h2>
        <div className="nm-spacer-48"></div>
        <form className="nm-layout" onSubmit={this.handleSubmit}>
          <Input
            field={this.state.constant_price}
            showError={true}
            onChange={this.handleChange} />
          <p className="nm-layout nm-link" onClick={this.addDiscountField}>
            <Add />
            Add Discount
          </p>
          <div>
            {
              this.state.discounts.values.map((discount, index) => {
                return (
                  <Input
                    key={index}
                    field={discount}
                    onRemove={() => this.removeArrayIndex(index, "discounts")}
                    showError={true}
                    onChange={(value) => this.setArrayState(index, { value }, "discounts")}/>
                )
              })
            }
          </div>
          <div className="nm-spacer-48"></div>

          {/* Change Subscription Frequency */}
          <div style={{display: 'inline-block'}}>
            <DropDown
              field={this.state.quantity}
              textExtractor={this._getDropdownText} 
              options={typeOptions.quantity}
              onChange={(option) => {this.handleDropdownClick(option, "quantity")}}
            />
            </div>
            <div style={{display: 'inline-block'}}>
            <DropDown
              field={this.state.shipping_interval_frequency}
              textExtractor={this._getDropdownText} 
              options={typeOptions.shipping_interval_frequency}
              onChange={(option) => {this.handleDropdownClick(option, "shipping_interval_frequency")}}
            />
          </div>
          <div className="nm-spacer-12"></div>
          {/* End Change Subscription Frequency */}
          <DatePicker
            selected={moment(this.state.next_ship_date.value)}
            onChange={this.handleDateChange}
            dateFormat="LLL"
            customInput={
            <Input 
              field={this.state.next_ship_date}
              onChange={this.handleChange}/> } />
          <h2 className="">
            Next 10 Skips
          </h2>
          <div className="nm-spacer-12"></div>
          <div className="nm-spacer-12"></div>
          {
            this.state.skips.values.map((skip, index) => {
              return (
                <Checkbox
                  key={index}
                  field={skip}
                  onChange={(value) => this.setArrayState(index, { value }, "skips")}/>
              )
            })
          }
          <div className="nm-spacer-48"></div>
          {
            this.state.error &&
            <p className="nm-form__error">Error: {this.state.error}</p>
          }
          <button type="submit" onClick={this.handleSubmit}>
            {this.props.methodText} Subscription
          </button>
        </form>
      </div>
    )
  }
}

export default SubscriptionForm;
