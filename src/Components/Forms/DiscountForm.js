import React, { Component } from 'react';
import moment from 'moment';
import config from '../../config'
import DatePicker from 'react-datepicker';
import Input from '../Input';
import Checkbox from '../Checkbox';
import DropDown from '../DropDown';
import formValidation from '../../Helpers/formValidation';

import 'react-datepicker/dist/react-datepicker.css';

const formFields = {
  id: {
    value: '',
    label: "Discount Name",
    name: "id",
    isRequired: true,
    validation: formValidation.validateEmpty
  },
  type_: {
    value: 'fixed',
    label: "Type",
    name: "type_",
    text: 'Fixed',
    isRequired: true
  },
  group: {
    values: config.groupOptions,
    label: "Products",
    type: "array",
    validation: formValidation.validateAtLeastOneCheckbox,
    isRequired: true
  },
  amount: {
    value: '',
    label: "Discount Value",
    name: "amount",
    isRequired: true,
    validation: formValidation.validateNumber
  },
  started_at: {
    value: moment().startOf('day'),
    label: "Start Date",
    name: "started_at",
    isRequired: true,
    isTime: true
  },
  ended_at: {
    value: moment().add(6, 'months').endOf('day'),
    label: "End Date",
    name: "ended_at",
    isTime: true
  },
  min_amount: {
    value: '',
    label: "Minimum Purchase Amount Required (required)",
    name: "min_amount",
    isRequired: true,
    validation: formValidation.validateNumber
  },
  user_apply_limit: {
    value: '',
    label: "User apply limit",
    name: "user_apply_limit",
    isRequired: true,
    validation: formValidation.validateNumber
  },
  total_apply_limit: {
    value: '',
    label: "Total apply limit",
    name: "total_apply_limit",
    validation: formValidation.validateNumber
  }
}

const typeOptions = [
  {
    value: "fixed",
    text: "Fixed"
  },
  {
    value: "percent",
    text: "Percent"
  }
]

class DiscountForm extends Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.stopModalClick = this.stopModalClick.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleDropdownClick = this.handleDropdownClick.bind(this);
    this.setArrayState = this.setArrayState.bind(this);
  }

  componentWillMount() {
    let fields = JSON.parse(JSON.stringify(formFields));
    if (this.props.initialFields) {
      for (let field of Object.keys(fields)) {
        if (this.props.initialFields[field]) {
          fields[field].value = this.props.initialFields[field];
        }
      }
      fields.min_amount.value = this.props.initialFields.rules.min_amount;
      fields.user_apply_limit.value = this.props.initialFields.rules.user_apply_limit;
      fields.total_apply_limit.value = this.props.initialFields.rules.total_apply_limit;
      for (let option of typeOptions) {
        if (fields.type_.value === option.value) {
          fields.type_.text = option.text;
        }
      }
      fields.group.values = fields.group.values.map((groupItem, index) => {
        return {
          name: `group_${index}`,
          label: groupItem.label,
          value: this.props.initialFields.group.indexOf(groupItem.value) > -1
        }
      })
      this.setState(Object.assign({error: undefined}, fields))
    } else {
      fields.group.values = fields.group.values.map((groupItem, index) => {
        return {
          name: `group_${index}`,
          label: groupItem.label,
          value: false
        }
      })
      this.setState(Object.assign({error: undefined}, fields))
    }
  }

  handleChange(val, name) {
    this.setState({
      [name]: {
        ...this.state[name],
        value: val
      }
    })
  }

  handleSubmit(e) {
    if (e) {
      e.preventDefault();
    }
    const { total_apply_limit } =  this.state;
    let error = formValidation.validate(this.state);
    if (error) {
      this.setState({error})
    }
    else {
      let body = {
        id: this.state.id.value,
        type_: this.state.type_.value,
        group: this.state.group.values.map((groupItem, index) => {
          if (groupItem.value) {
            return { id: config.groupOptions[index].value }
          }
        }).filter(groups => groups),
        amount: parseFloat(this.state.amount.value),
        started_at: parseInt(moment(this.state.started_at.value).startOf('day').valueOf()),
        ended_at: parseInt(moment(this.state.ended_at.value).endOf('day').valueOf()),
        rules: {
          min_amount: parseFloat(this.state.min_amount.value),
          user_apply_limit: parseInt(this.state.user_apply_limit.value),
          total_apply_limit: total_apply_limit.value === "" ? undefined : parseInt(total_apply_limit.value)
        }
      }
      this.props.onSubmit(body)
      this.setState({
        error: undefined
      })
    }
  }

  handleDateChange(date, field) {
    this.setState({
      [field]: {
        ...this.state[field],
        value: moment(date).startOf('day')
      }
    })
  }

  stopModalClick(e) {
    e.stopPropagation()
  }

  handleDropdownClick(option, field) {
    this.setState({
      [field]: {
        ...this.state[field],
        value: option.value,
        text: option.text
      }
    })
  }

  _getDropdownText(field) {
    return field.text;
  }

  setArrayState(index, newObj, field) {
    this.setState({
      [field]: {
        ...this.state[field],
        values: [
          ...this.state[field].values.slice(0, index),
          Object.assign({}, this.state[field].values[index], newObj),
          ...this.state[field].values.slice(index + 1)
        ]
      }
    })
  }

  render() {
    return (
      <div className="nm-layout" onClick={this.stopModalClick} style={{marginTop: 20, marginBottom: 20}}>
        <h2>Discount {this.props.methodText}</h2>
        <form className="nm-layout" onSubmit={this.handleSubmit}>
          {
            this.props.initialFields &&
            <h2>{this.state.id.value}</h2>
          }
          {
            !this.props.initialFields &&
            <Input 
              field={this.state.id}
              onChange={this.handleChange}/>
          }
          <div className="nm-spacer-12"></div>
          <DropDown
            field={this.state.type_}
            textExtractor={this._getDropdownText} 
            options={typeOptions}
            onChange={(option) => {this.handleDropdownClick(option, "type_")}} />
          <h2 className="">
            Products
          </h2>
          <div className="nm-spacer-12"></div>
          <div className="nm-spacer-12"></div>
          {
            this.state.group.values.map((groupItem, index) => {
              return (
                <Checkbox
                  key={index}
                  field={groupItem}
                  onChange={(value) => this.setArrayState(index, { value }, "group")}/>
              )
            })
          }
          <div className="nm-spacer-12"></div>
          <Input 
            field={this.state.amount}
            onChange={this.handleChange}/>
          <Input 
            field={this.state.min_amount}
            onChange={this.handleChange}/>
          <Input 
            field={this.state.user_apply_limit}
            onChange={this.handleChange}/>
          <Input 
            field={this.state.total_apply_limit}
            onChange={this.handleChange}/>
          <DatePicker
            selected={moment(this.state.started_at.value)}
            onChange={(date) => { this.handleDateChange(date, "started_at") }}
            dateFormat="LLL"
            customInput={
            <Input 
              field={this.state.started_at}
              onChange={this.handleChange}/> } />
          <DatePicker
            selected={moment(this.state.ended_at.value)}
            onChange={(date) => { this.handleDateChange(date, "ended_at") }}
            dateFormat="LLL"
            customInput={
            <Input 
              field={this.state.ended_at}
              onChange={this.handleChange}/> } />
          {
            this.state.error &&
            <p className="nm-form__error">Error: {this.state.error}</p>
          }
          <button type="submit" onClick={this.handleSubmit}>
            {this.props.methodText} Discount
          </button>
        </form>
      </div>
    )
  }
}

export default DiscountForm;
