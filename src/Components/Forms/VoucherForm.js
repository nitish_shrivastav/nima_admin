import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, submit, reduxForm, change } from 'redux-form';
import moment from 'moment';
import _ from 'lodash';

import Button from '../Button';
import config from '../../config'
import DateField from '../ReduxFormFields/DateField';
import DatePicker from 'react-datepicker';
import Input from '../Input';
import InputField from '../ReduxFormFields/InputField';
import Checkbox from '../ReduxFormFields/Checkbox';
import DropDown from '../DropDown';
import formValidation from '../../Helpers/formValidation';
import { notEmptyValidation, numberValidation,
  dateValidation, positiveNumberValidation } from '../../Helpers/reduxFormValidation';

import 'react-datepicker/dist/react-datepicker.css';

let VoucherForm = (props) => {
  return (
    <div>
      <h2>Voucher Update</h2>
      <form className="nm-layout">
        <label className="nm-form__label">
          Products
        </label>
        {
          config.groupOptions.map((option) => {
            let str = `line_items.voucherkey_${option.value}`;
            return (
              <Field
                label={option.label}
                name={`line_items.voucherkey_${option.value}`}
                component={Checkbox}
              />
            )
          })
        }
        <div className="nm-spacer-12"></div>
        <Field
          name="start_date"
          label="Start date"
          validate={[dateValidation]}
          onDateChange={(value) => {props.dispatch(change('voucherForm', 'start_date', value))}}
          component={DateField}
        />
        <Field
          name="end_date"
          label="End date"
          validate={[dateValidation]}
          onDateChange={(value) => {props.dispatch(change('voucherForm', 'end_date', value))}}
          component={DateField}
        />
        <div className="nm-spacer-12"></div>
        <div className="nm-clear"></div>
        <Button
          text="Submit"
          onClick={() => props.dispatch(submit('voucherForm'))}
          center={true}
          style={{maxWidth: 300}}
        />
      </form>
    </div>
  );
}

VoucherForm = reduxForm({
  form: 'voucherForm'
})(VoucherForm);

export default connect(
  (state) => ({
    initialValues: {
      start_date: _.get(state, 'voucher.detail.start_date'),
      end_date: _.get(state, 'voucher.detail.end_date'),
      create_s3_csv: _.get(state, 'voucher.detail.create_s3_csv') || true,
      line_items: _.get(state, 'voucher.detail.line_items') && state.voucher.detail.line_items.reduce((acc, curr) => {
        acc[`voucherkey_${curr.variant_id}`] = true
        return acc;
      }, {})
    },
    voucher: state.detail
  })
)(VoucherForm);
