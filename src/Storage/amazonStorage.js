import moment from 'moment';

const amazonStorage = {};

amazonStorage.getAmazonUser = () => {
  let awsUser = JSON.parse(localStorage.getItem('aws-user'));
  if (awsUser && moment(awsUser.tokens.Expiration) > moment()) return awsUser;
  else return null;
}

amazonStorage.setAmazonUser = (awsUser) => {
  localStorage.setItem('aws-user', JSON.stringify(awsUser))
}

amazonStorage.clearAmazonUser = () => {
  localStorage.setItem('aws-user', null);
}

export default amazonStorage;