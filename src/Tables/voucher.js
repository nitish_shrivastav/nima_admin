import timeHelper from '../Helpers/timeHelper';

export default [
  {
    name: "Voucher ID",
    prepData: (data) => data.id
  },
  {
    name: "Started at",
    prepData: (data) => timeHelper.formatMomentWithTimezone(data.start_date)
  },
  {
    name: "Ended at",
    prepData: (data) => timeHelper.formatMomentWithTimezone(data.end_date)
  }
]
