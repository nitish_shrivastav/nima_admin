export default [
  {
    name: "EMAIL",
    field: "email"
  },
  {
    name: "FIRST",
    field: "first_name"
  },
  {
    name: "LAST",
    field: "last_name"
  }
]