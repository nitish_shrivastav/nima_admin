import moment from 'moment';
import timeHelper from '../Helpers/timeHelper';

export default [
  {
    name: "Discount Name",
    prepData: (data) => {
      return data.id
    }
  },
  {
    name: "Amount",
    prepData: (data) => {
      if (data.type_ === "percent") {
        return `${data.amount}%`
      }
      else {
        return `$${data.amount}`
      }
    }
  },
  {
    name: "Started at",
    prepData: (data) => {
      return timeHelper.formatMomentWithTimezone(data.started_at);
    }
  },
  {
    name: "Ended at",
    prepData: (data) => {
      return timeHelper.formatMomentWithTimezone(data.ended_at);
    }
  },
  {
    name: "Total Uses",
    prepData: (data) => {
      return data.use_count
    }
  },
  {
    name: "Total Left",
    prepData: (data) => {
      if (data.rules && data.rules.total_apply_limit) {
        return data.rules.total_apply_limit - data.use_count 
      }
      else {
        return "∞"
      }
    }
  }
]