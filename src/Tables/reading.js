import moment from 'moment';
import readingHelper from '../Helpers/readingHelper';
import timeHelper from '../Helpers/timeHelper';

export default [
  {
    name: "Test Date",
    prepData: (data) => {
      return timeHelper.formatMomentWithTimezone(data.local_time);
    }
  },
  {
    name: "Firmware Time",
    prepData: (data) => {
      return timeHelper.formatMomentWithoutTimezone(data.fw_time);
    }
  },
  {
    name: "Reviewed",
    prepData: (data) => {
      if (data.review_id) return "Yes";
      else return "No";
    }
  },
  {
    name: "Result",
    prepData: (data) => {
      return `${readingHelper.mapReadingToTextResult(data.test_result, data.allergen)} ${readingHelper.mapReadingDetailToText(data.result_detail) && readingHelper.mapReadingDetailToText(data.result_detail).status !== "N/A" ?
                                                "| " + readingHelper.mapReadingDetailToText(data.result_detail).status :
                                                ""}`;
    }
  },
  {
    name: "Deleted At",
    prepData: (data) => {
      if (data.deleted_at) return timeHelper.formatMomentWithoutTimezone(parseInt(data.deleted_at));
      else return "";
    }
  }
]
