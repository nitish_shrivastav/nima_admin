import moment from 'moment';
import timeHelper from '../Helpers/timeHelper';

export default [
  {
    name: "Shopify Customer Id",
    prepData: (data) => {
      return data.id
    }
  },
  {
    name: "Shopify Order Id",
    prepData: (data) => {
      return data.code_order.split('|')[1]
    }
  },
  {
    name: "Order Time",
    prepData: (data) => {
      return timeHelper.formatMomentWithTimezone(data.local_time);
    }
  }
]
