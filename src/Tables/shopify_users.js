import moment from 'moment';
import timeHelper from '../Helpers/timeHelper';

const toTitleCase = (str) => {
  return str.replace(/\w\S*/g, function(txt){
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

export default [
  {
    name: "Name",
    prepData: (data) => {
      return `${data.first_name} ${data.last_name}`;
    }
  },
  {
    name: "Email",
    prepData: (data) => {
      return data.email
    }
  }
]