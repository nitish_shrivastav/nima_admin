import moment from 'moment';
import timeHelper from '../Helpers/timeHelper';
import variantHelper from '../Helpers/variantHelper';


export default [
  {
    name: "Item",
    prepData: (data) => {
      let variant_id = data.order.split('|')[1];
      return variantHelper.getVariantName(variant_id);
    }
  },
  {
    name: "Send Day",
    prepData: (data) => {
      return `${data.shipping_interval_frequency}:${data.send_day}`
    }
  },
  {
    name: "Created At",
    prepData: (data) => {
      return timeHelper.formatMomentWithoutTimezone(data.created_at);
    }
  },
  {
    name: "Deleted At",
    prepData: (data) => {
      if (data.deleted_at) {
        return timeHelper.formatMomentWithoutTimezone(data.deleted_at);
      }
      else {
        return "";
      }
    }
  }
]