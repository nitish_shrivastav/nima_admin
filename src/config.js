import prodConfig from './config.prod';
import stgConfig from './config.stg';
import devConfig from './config.dev';

let config = devConfig;
if (process.env.REACT_APP_NIMA_ENV && process.env.REACT_APP_NIMA_ENV === "staging") {
  config = stgConfig;
}
else if (process.env.REACT_APP_NIMA_ENV && process.env.REACT_APP_NIMA_ENV === "production") {
  config = prodConfig;
}

config.version = "2.1.5";

export default config;
