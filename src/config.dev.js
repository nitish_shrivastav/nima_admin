export default {
  api_url: "https://4x486f0t48.execute-api.us-west-2.amazonaws.com/dev",
  store_front_api_url: "https://gwrbuiqj9i.execute-api.us-west-2.amazonaws.com/dev",
  host: "api-dev.nimasensor.com",
  api_version: "",
  service: "execute-api",
  region: "us-west-2",
  shopify_domain: "https://nima-dev3.myshopify.com",
  groupOptions: [
    {
      value: "",
      label: ""
    },
    {
      value: "all",
      label: "All Customers"
    },
    {
      value: "357681397766",
      label: "Gluten Premium Membership"
    },
    {
      value: "8112774512752",
      label: "Peanut Premium Membership"
    },
    {
      value: "31763432838",
      label: "Gluten Capsule Pack - One 12-pack every other month"
    },
    {
      value: "31763539974",
      label: "Gluten Capsule Pack - One 12-pack per month"
    },
    {
      value: "31763540038",
      label: "Gluten Capsule Pack - Two 12-packs per month"
    },
    {
      value: "31764509958",
      label: "Gluten Capsule Pack - One time purchase"
    },
    {
      value: "8112399843440",
      label: "Peanut Capsule Pack - One 12-pack every other month"
    },
    {
      value: "8112399810672",
      label: "Peanut Capsule Pack - One 12-pack per month"
    },
    {
      value: "8112400007280",
      label: "Peanut Capsule Pack - Two 12-packs per month"
    },
    {
      value: "8112400892016",
      label: "Peanut Capsule Pack - One time purchase"
    },
    {
      value: "752584294406",
      label: "Sensor Only - Gluten"
    },
    {
      value: "8112779919472",
      label: "Sensor Only - Peanut"
    },
    {
      value: "50231449542",
      label: "Gluten Starter Kit"
    },
    {
      value: "589571751942",
      label: "Peanut Starter Kit"
    },
    {
      value: "14050656911423",
      label: "Gluten Mini Bundle"
    },
    {
      value: "14050805284927",
      label: "Peanut Mini Bundle"
    },
    {
      value: "14051012149311",
      label: "Gluten Mini Bundle VOUCHER"
    },
    {
      value: "14050824454207",
      label: "Peanut Mini Bundle VOUCHER"
    }
  ]
}
