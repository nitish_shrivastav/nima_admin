import React from 'react'
import config from './config';
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import store, { history } from './store'
import App from './Containers/App';

import 'sanitize.css/sanitize.css'
import './index.scss'

const target = document.querySelector('#root')

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <p className="nm-layout">
          V {config.version}
        </p>
        <App />
      </div>
    </ConnectedRouter>
  </Provider>,
  target
)
