import request from './Request';
import config from '../config';

const apiUrl = `${config.api_url}/store`;

class DiscountService {
  static list(params) {
    return request.protectedGet(`${apiUrl}/discounts`, params)
  }

  static retrieve(id) {
    return request.protectedGet(`${apiUrl}/discounts/info`, { id })
  }

  static create(body) {
    return request.protectedPost(`${apiUrl}/discounts`, body)
  }

  static bulkCreate(body) {
    return request.protectedPost(`${apiUrl}/discounts/bulk_create`, body);
  }

  static update(body) {
    return request.protectedPut(`${apiUrl}/discounts`, body);
  }
}

export default DiscountService;
