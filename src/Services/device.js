import request from './Request';
import config from '../config';

const service = {}
const apiUrl = `${config.api_url}${config.api_version}`;

service.list = (user_id) => {
  return request.protectedGet(`${apiUrl}/devices`, { user_id })
};

service.retrieve = (id) => {
  return request.protectedGet(`${apiUrl}/devices/${id}/`)
};

export default service;