import request from './Request';
import config from '../config';

const service = {};
const apiUrl = `${config.api_url}${config.api_version}`;

service.list = (params) => {
  return request.protectedGet(`${apiUrl}/admin/users`, params);
};

service.retrieve = (user_id) => {
  return request.protectedGet(`${apiUrl}/users/info`, { user_id });
};

service.forgotPass = (email) => {
  return request.protectedPost(`${apiUrl}/users/forgot`, {email: email});
};

service.resendVerif = (email) => {
  return request.protectedPost(`${apiUrl}/users/resend-verify`, {email: email});
}

export default service;
