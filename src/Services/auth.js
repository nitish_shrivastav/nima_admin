import request from './Request';
import config from '../config';

const service = {}
const apiUrl = `${config.api_url}${config.api_version}`;

service.login = (body) => {
  return request.post(`${apiUrl}/users/login/`, body)
};

service.refresh = (body) => {
  return request.post(`${apiUrl}/users/refresh/`, body)
};

export default service;