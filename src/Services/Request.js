  //_convertParametersToString(queryParameters) {
  //  let keys = Object.keys(queryParameters).sort();
  //  let qs = ""; 

  //  for (let i = 0; i < keys.length; i++) {
  //    qs += keys[i] + "=" + querystring.escape(queryParameters[keys[i]]);
  //    if (i !== (keys.length - 1)) qs += "&";
  //  }
  //  return qs;
  //}


import queryString from 'query-string';
import moment from 'moment';
import crypto from 'crypto';
import config from '../config';
import amazonStorage from '../Storage/amazonStorage';

// ---------------------------------------------------------------------------
// These functions are the basic request functions used to interact with
// api's with fetch, urls need to be passed in accordingly.
// ---------------------------------------------------------------------------
let request = {};

let tokenString = 'Token token=' + config.developer_key;

function handleErrors(res) {
  if (res.ok) {
    return res;
  }
  return res.json()
  .then(err => {
    if (err.hasOwnProperty('message')) {
      throw new Error(err.message);
    } else {
      throw new Error('An unknown error occurred');
    }
  })
}

const throwError = (err) => {
  return new Promise((resolve, reject) => {
    resolve(err)
  }).then((err) => {
    throw new Error(err)
  })
}

const _sign = (key, msg, encoding) => {
  return crypto.createHmac('sha256', key).update(msg, 'utf8').digest(encoding);
}

const _hash = (msg, encoding) => {
  return crypto.createHash('sha256').update(msg, 'utf8').digest(encoding);
}

const _getAmazonSigningKey = (secretKey, datestamp, region, service, stringToSign) => {
  let signedDate = _sign('AWS4' + secretKey, datestamp);
  let signedRegion = _sign(signedDate, region);
  let signedService = _sign(signedRegion, service);
  let signedCredentials = _sign(signedService, "aws4_request");
  return _sign(signedCredentials, stringToSign, "hex");
}

const _getCanonicalQueryString = (params) => {
  return Object.keys(params).sort().map(function(key) {
    return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
  }).join('&');
}


const _getAmazonRequestOptions = (method, keys, uri, canonicalQuerystring, body)  => {
  let timestamp = moment();
  let REGION = "us-west-2";
  let SERVICE = "execute-api";
  let ymdTimestamp = timestamp.utc().format("YYYYMMDD");
  let ymdhmsTimestamp = timestamp.utc().format('YYYYMMDDTHHmmss') + "Z";
  let requestHeaders = {
    "method": method,
    "host": config.host,
    "service": SERVICE,
    "region": REGION,
    "headers": {
      "host": config.host,
      "x-amz-date": ymdhmsTimestamp,
      "x-amz-security-token": keys.tokens.SessionToken,
      "content-type": "application/json"
    }
  }

  let payloadHash = _hash('', 'hex');
  if (body) {
    payloadHash = _hash(JSON.stringify(body), 'hex');
    requestHeaders.body = JSON.stringify(body);
  }
  let canonicalUri = uri;
  let canonicalHeaders = `content-type:application/json\nhost:${config.host}\nx-amz-date:${ymdhmsTimestamp}\nx-amz-security-token:${keys.tokens.SessionToken}\n`;
  let credentialScope = `${ymdTimestamp}/${REGION}/${SERVICE}/aws4_request`;
  let signedHeaders = "content-type;host;x-amz-date;x-amz-security-token";

  let canonicalRequest = `${method}\n${canonicalUri}\n${canonicalQuerystring}\n${canonicalHeaders}\n${signedHeaders}\n${payloadHash}`;
  let stringToSign = `AWS4-HMAC-SHA256\n${ymdhmsTimestamp}\n${credentialScope}\n${_hash(canonicalRequest, 'hex')}`;
  let signature = _getAmazonSigningKey(keys.tokens.SecretAccessKey, ymdTimestamp, REGION, SERVICE, stringToSign);
  let authorizationString = `AWS4-HMAC-SHA256 Credential=${keys.tokens.AccessKeyId}/${credentialScope}, SignedHeaders=${signedHeaders}, Signature=${signature}`;

  requestHeaders.headers.authorization = authorizationString;
  return requestHeaders;
}

request.get = function (url, params) {
  let apiUrl = `${config.api_url}${config.api_version}${url}`;
  if (params) {
    apiUrl = `${apiUrl}?${queryString.stringify(params)}`;
  }
  const fetchOptions = {
    method: 'GET',
    headers: {
      Accept: 'application/json'
    }
  };
  return fetch(apiUrl, fetchOptions)
    .then(handleErrors)
    .then(res=>res.json());
};

request.protectedGet = function (url, params) {
  if (!params) params = {};
  let canonicalQuerystring = _getCanonicalQueryString(params);
  let apiUrl = `${url}?${canonicalQuerystring}`;
  let tokens = amazonStorage.getAmazonUser();
  if (!tokens) {
    return throwError('Error, token must exist')
  } else {
    let route = url.split(config.host)[1]
    let fetchOptions = _getAmazonRequestOptions("GET", tokens, route, canonicalQuerystring);
    return fetch(apiUrl, fetchOptions)
    .then(handleErrors)
    .then(res=>res.json());
  }
};

request.post = function (apiUrl, body) {
  let fetchOptions = {
    method: 'POST',
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(body)
  };
  return fetch(apiUrl, fetchOptions)
  .then(handleErrors)
  .then(res=>res.json())
};

request.protectedPost = function (url, body) {
  let tokens = amazonStorage.getAmazonUser();
  let route = url.split(config.host)[1]
  if (false) {
    return throwError('Error, token must exist')
  } else {
    let fetchOptions = _getAmazonRequestOptions("POST", tokens, route, '', body);
    return fetch(url, fetchOptions)
    .then(handleErrors)
    .then(res=>res.json())
  }
};

request.upload = function (apiUrl, data, method = 'POST', needsAuth = true) {
  let body = new FormData()

  for (let key in data) {
    body.append(key, data[key])
  }

  let token = localStorage.getItem('token')
  if (token == null && needsAuth == true) {
    return throwError('Error, token must exist')
  } else {
    let fetchOptions = {
      method: method,
      headers: {
        Authorization: needsAuth ? tokenString + ':' + token : tokenString,
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data; boundary=6ff46e0b6b5148d984f148b6542e5a5d',
      },
      body: body
    };
    return fetch(apiUrl, fetchOptions)
    .then(handleErrors)
    .then(res => res.json());
  }
};

request.patch = function (apiUrl, body, needsAuth = true) {
  let token = localStorage.getItem('token')
  if (token == null && needsAuth == true) {
    return throwError('Error, token must exist')
  } else {
    let fetchOptions = {
      method: 'PATCH',
      headers: {
        Accept: 'application/json',
        Authorization: needsAuth ? tokenString + ':' + token : tokenString,
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: body
      // body: JSON.stringify(body)
    };
    return fetch(apiUrl, fetchOptions)
    .then(handleErrors)
    .then(res=>res.json());
  }
};

request.put = function (apiUrl, body, needsAuth = true) {
  let token = localStorage.getItem('token')
  if (token == null && needsAuth == true) {
    return throwError('Error, token must exist')
  } else {
    let fetchOptions = {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        Authorization: needsAuth ? tokenString + ':' + token : tokenString,
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: body
      // body: JSON.stringify(body)
    };
    return fetch(apiUrl, fetchOptions)
    .then(handleErrors)
    .then(res=>res.json());
  }
};

request.protectedPut = function (url, body) {
  let tokens = amazonStorage.getAmazonUser();
  let route = url.split(config.host)[1]
  if (!tokens) {
    return throwError('Error, token must exist')
  } else {
    let fetchOptions = _getAmazonRequestOptions("PUT", tokens, route, '', body);
    return fetch(url, fetchOptions)
    .then(handleErrors)
    .then(res=>res.json())
  }
};

request.delete = function (apiUrl, needsAuth = true) {
  let token = localStorage.getItem('token')
  if (token == null && needsAuth == true) {
    return throwError('Error, token must exist')
  } else {
    let fetchOptions = {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        Authorization: needsAuth ? tokenString + ':' + token : tokenString,
      }
    };
    return fetch(apiUrl, fetchOptions)
    .then(handleErrors)
    .then();
  }
};

request.protectedDelete = function (url, body) {
  let tokens = amazonStorage.getAmazonUser();
  let route = url.split(config.host)[1]
  if (!tokens) {
    return throwError('Error, token must exist')
  } else {
    let fetchOptions = _getAmazonRequestOptions("DELETE", tokens, route, '', body);
    return fetch(url, fetchOptions)
    .then(handleErrors)
    .then(res=>res.json())
  }
};


export default request;