import request from './Request';
import config from '../config';


class VoucherService {

  static list(params) {
    return request.protectedGet(`${config.api_url}/store/admin/vouchers`, params)
  }

  static retrieve(voucher_id) {
    return request.protectedGet(`${config.api_url}/store/admin/vouchers/${voucher_id}`)
  }

  static bulkCreate(body) {
    return request.protectedPost(`${config.api_url}/store/admin/vouchers/bulk_create`, body);
  }

  static update(id, body) {
    return request.protectedPut(`${config.api_url}/store/admin/vouchers/${id}`, body);
  }

  static delete(body) {
    return request.protectedDelete(`${config.api_url}/store/admin/vouchers/${body.id}`, body);
  }

}

export default VoucherService;