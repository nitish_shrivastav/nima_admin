import request from './Request';
import config from '../config';

const service = {}
const apiUrl = `${config.api_url}/store`;

service.list = (params) => {
  return request.protectedGet(`${apiUrl}/admin/subscriptions`, params)
};

service.retrieve = (user_id) => {
  return request.protectedGet(`${apiUrl}/admin/subscriptions/info`, { user_id })
};

service.update = (body) => {
  return request.protectedPut(`${apiUrl}/admin/subscriptions`, body)
};

service.destroy = (body) => {
  return request.protectedDelete(`${apiUrl}/admin/subscriptions`, body)
};

export default service;