import moment from 'moment';

const timeHelpers = {};

timeHelpers.formatMomentWithTimezone = (timestamp) => {
  let timezoneOffset = parseInt(moment(timestamp).format("ZZ").substring(0, 3))
  return moment(timestamp).add(timezoneOffset, 'h').format("h:mmA MM/DD/YY (Z)")
}

timeHelpers.formatMomentWithoutTimezone = (timestamp) => {
  return moment(timestamp).format("h:mmA MM/DD/YY")
}

timeHelpers.formatMomentDateWithoutTimezone = (timestamp) => {
  return moment(timestamp).utc().format("MM/DD/YY")
}

timeHelpers.dayFormat = (timestamp) => {
  return moment(timestamp).format("MMM Do, YYYY")
}

/** dayOfMonth() - converts number to 1st 2nd ... 30th 31st
 * @param {number|number as string} number - i.e. '30' or 30
 */
timeHelpers.dayOfMonth = (number) => {
  return moment().date(number).format('Do');
}

export default timeHelpers;
