import textHelper from '../Helpers/textHelpers';

const readingHelper = {};

readingHelper.mapReadingToTextResult = (readingValue, allergen) => {
  allergen = textHelper.uppercaseFirstLetter(allergen);
  const testResultMap = {
    0: `Test Error (${allergen})`,
    1: `${allergen} Found`,
    2: `${allergen} Found`,
    3: `${allergen} Free`
  };
  return  testResultMap[readingValue];
};

readingHelper.mapReadingDetailToText = (resultDetail) => {
  if (!resultDetail){
    return {status: "N/A", description: "N/A"}
  }
  else{
    const TEST_MAPPINGS = [
      {
        status: "Successful test",
        description: "There is no additional status information associated with this test."
      },
      {
        status: "Saturated camera",
        description: "The camera image is saturated. Possibly a camera malfunction."
      },
      {
        status: "Pre-image failed",
        description: "Test may have failed due to a used capsule being run, the Nima Partners  window is dirty, or there is some other type of optical distortion"
      },
      {
        status: "Slow flow",
        description: "The test failed due to a 'slow flow' condition. User possibly overpacked the capsule with food."
      },
      {
        status: "No control line",
        description: "No control line was found. Possibly a capsule with bad chemistry of the valve failed to fire (underdeveloped strip)"
      },
      {
        status: "No capsule",
        description: "The capsule was removed early from the Nima Partners ."
      },
      {
        status: "Decoupled mixing",
        description: "Mixing did not complete properly. Capsule may not have been fully closed."
      },
      {
        status: "Poor mix",
        description: "Poor mixing was detected, and a gluten free test result was found. Results are inconclusive."
      }
    ];
    return  TEST_MAPPINGS[resultDetail];
  }
};

export default readingHelper;
