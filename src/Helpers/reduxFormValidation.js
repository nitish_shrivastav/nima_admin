export const notEmptyValidation = (value) => {
  if (!value || value === "") {
    return "Invalid value";
  }
  else {
    return undefined;
  }
}

export const numberValidation = (value) => {
  if (isNaN(value)) {
    return "Invalid value, must be a number";
  }
  else {
    return undefined;
  }
}

export const positiveNumberValidation = (value) => {
  if (isNaN(value)) {
    return "Invalid value, must be a number";
  }
  else if (value < 0) {
    return "Invalid value, must be positive";
  }
  else {
    return undefined;
  }
}

export const dateValidation = (value) => {
  if (isNaN(value) || value.toString().length !== 13) {
    return "Invalid date";
  }
  else {
    return undefined;
  }
}

export const phoneValidation = (value) => {
  if (!value || !/^(0|[1-9][0-9]{9})$/i.test(value)) {
    return 'Invalid phone number, must be 10 digits';
  }
  else {
    return undefined;
  }
}

export const emailValidation = (value) => {
  if (!value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    return 'Invalid email address';
  }
  else {
    return undefined;
  }
}
