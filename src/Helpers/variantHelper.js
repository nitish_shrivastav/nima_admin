import config from '../config';
const variantHelpers = {};

variantHelpers.getVariantName = (variant_id) => {
  for (let option of config.groupOptions) {
    if (option.value === variant_id) {
      return option.label;
    }
  }
  return variant_id;
}

export default variantHelpers;
