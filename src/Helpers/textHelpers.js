const textHelpers = {};

textHelpers.uppercaseFirstLetter = (word) => {
  return word ? word.charAt(0).toUpperCase() + word.slice(1) : ""
}

export default textHelpers;