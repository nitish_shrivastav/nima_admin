const formValidation = {};

formValidation.validateEmpty = (field) => {
  if (field.value === '') {
    return `${field.label} must be filled out`;
  } else {
    return false;
  }
}

formValidation.validateNumber = (field) => {
  if (field.value !== "" && !isNaN(Number(field.value))) {
    return false;
  }
  else if ((!field.value || field.value === "") && !field.isRequired) {
    return false;
  }
  else {
    return `${field.label} must be a valid number`;
  }
}

formValidation.validateAtLeastOneCheckbox = (field) => {
  for (let checkbox of field.values) {
    if (checkbox.value) {
      return false;
    }
  }
  return `${field.label} requires at least one selection`
}

formValidation.validate = (fields) => {
  for (let field of Object.keys(fields)) {
    if (fields[field] && fields[field].validation) {
      return fields[field].validation(fields[field])
    }
  }
  return false;
}

export default formValidation;