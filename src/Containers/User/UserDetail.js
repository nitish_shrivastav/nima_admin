import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import _ from 'lodash';
import readingActions from '../../Actions/reading';
import reviewActions from '../../Actions/review';
import Table from '../../Components/Table';
import readingTable from '../../Tables/reading';
import LabeledRow from '../../Components/LabeledRow';
import userActions from '../../Actions/user';
import deviceActions from '../../Actions/device';
import mobileDeviceActions from '../../Actions/mobileDevice';
import TypeSelect from '../../Components/TypeSelect';
import Button from '../../Components/Button'

const TableType = [
  {
    display: "Readings with reviews",
    value: "reviews"
  },
  {
    display: "All Readings",
    value: "readings"
  }
]


class UserDetail extends Component {
  constructor() {
    super();
    this.getDetailRoute = this.getDetailRoute.bind(this);
    this.getTestErrorRate = this.getTestErrorRate.bind(this);
    this.getFirmwares = this.getFirmwares.bind(this);
    this.getDeviceList = this.getDeviceList.bind(this);
    this.handleTypeClick = this.handleTypeClick.bind(this);
    this.sendForgotPassword = this.sendForgotPassword.bind(this);
    this.resendVerification = this.resendVerification.bind(this);
  }

  componentWillMount() {
    const { dispatch, match, user } = this.props;
    this.setState({
      loginMethod: match.params.user_id.indexOf('facebook') > -1 ? "Facebook" : "Email",
      selectedTableType: TableType[0].value
    });
    dispatch(userActions.retrieve(match.params.user_id));
    dispatch(readingActions.list(match.params.user_id));
    dispatch(deviceActions.list(match.params.user_id));
    dispatch(mobileDeviceActions.list(match.params.user_id));
    dispatch(reviewActions.list(match.params.user_id));
  }

  getDetailRoute(reading) {
    return `/users/${this.props.match.params.user_id}/readings/${reading.id}/`;
  }

  getTestErrorRate() {
    let readingList = this.props.review.readingList.concat(this.props.reading.list);
    let readingCount = readingList.length === 0 ? 1 : readingList.length;
    let errorCount = 0;
    for (let read of readingList) {
      if (read.test_result === 0) {
        errorCount = errorCount + 1;
      }
    }
    return `${((errorCount / readingCount) * 100).toFixed(1)}%`;
  }

  sortRowsByDate(readingList) {
    return _.sortBy(readingList, ['local_time']).reverse();
  }

  getFirmwares(devices) {
    let firmwareList = "";
    if (!devices[0]) return "xxxxxx";
    else {
      devices = _.orderBy(devices, ['updated_at'], ['desc']);
      for (let device of devices) {
        firmwareList = firmwareList + device.fw_version + ", ";
      }
      return firmwareList.substring(0, firmwareList.length - 2);
    }
  }

  getDeviceList(devices) {
    let deviceList = "";
    if (!devices[0]) return "xxxxxx";
    else {
      devices = _.orderBy(devices, ['updated_at'], ['desc']);
      for (let device of devices) {
        deviceList = deviceList + device.device_sn + ", ";
      }
      return deviceList.substring(0, deviceList.length - 2);
    }
  }

  handleTypeClick(item) {
    this.setState({ selectedTableType: item.value });
  }

  sendForgotPassword() {
    const { dispatch, user } = this.props;
    dispatch(userActions.sendForgotPassword(user.detail.email));
  }

  resendVerification() {
    const { dispatch, user } = this.props;
    dispatch(userActions.resendVerification(user.detail.email));
  }

  render() {
    const { reading, review, user, device, mobileDevice } = this.props;
    let timeFormat = "MMM Do YYYY, h:mm:ss a"
    return (
      <div>
        <div className="nm-layout">
          <Link to="/" className="nm-link nm-pull--left">
            &larr; Back to all customers
          </Link>
        </div>
        {
          user.detail &&
          <div>
            <h1 className="nm-layout">
              { user.detail.first_name } { user.detail.last_name }
            </h1>
            <p className="nm-layout">
              { user.detail.email }
            </p>
            <div className="nm-spacer-12"/>
            <button type="submit" className="nm-layout" style={{ marginLeft: '0px', color: 'grey', maxWidth: '250px' }} onClick={this.resendVerification}>
              Resend Account Verification
            </button>
            <div className="nm-spacer-12"/>
            <button type="submit" className="nm-layout" style={{ marginLeft: '0px', color: 'grey', maxWidth: '250px' }} onClick={this.sendForgotPassword}>
              Send Forgot Password
            </button>
            <div className="nm-spacer-48"/>
            <div className="nm-layout">
              <div className="nm-layout nm-layout__column--80">
                <LabeledRow
                  column1Label="FIRMWARE VERSION"
                  column1Text={this.getFirmwares(device.list)}
                  column2Label="DEVICE SERIAL NUMBER"
                  column2Text={this.getDeviceList(device.list)}/>
                <LabeledRow
                  column1Label="APP VERSION"
                  column1Text="xxxxxx"
                  column2Label="OS Type"
                  column2Text={mobileDevice.list[0] ? `${mobileDevice.list[0].os_type} ${mobileDevice.list[0].os_version}` : "Unknown"} />
                <LabeledRow
                  column1Label="LAST LOGIN"
                  column1Text={user.detail && user.detail.last_login ? moment(user.detail.last_login).format(timeFormat) : "xxxxxx"}
                  column2Label="LOGIN METHOD"
                  column2Text={`${this.state.loginMethod} ${user.detail.email_verified ? "(verified)" : "(NOT verified)"}`}/>
                <LabeledRow
                  column1Label="SIGNUP DATE"
                  column1Text={user.detail && user.detail.created_at ? moment(user.detail.created_at).format(timeFormat) : "xxxxxx"}
                  column2Label="LAST PAIR DATE"
                  column2Text={mobileDevice.list[0] ? moment(mobileDevice.list[0].updated_at).format(timeFormat) : "xxxxxx"} />
                <LabeledRow
                  column1Label="TEST ERROR RATE"
                  column1Text={this.getTestErrorRate()}
                  column2Label="NUMBER OF TESTS"
                  column2Text={reading.list.length}/>
                <LabeledRow
                  column1Label="NUMBER OF REVIEWS"
                  column1Text={review.list.length} />
              </div>
            </div>
            <div className="nm-spacer-48"/>
          </div>
        }
        <TypeSelect
          onTypeClick={this.handleTypeClick}
          selected={this.state.selectedTableType}
          name="userdetail-select"
          items={TableType}/>
        {
          this.state.selectedTableType === "reviews" &&
          <Table
            rows={this.sortRowsByDate(review.readingList)}
            setDetailRoute={this.getDetailRoute}
            columns={readingTable}/>
        }
        {
          this.state.selectedTableType === "readings" &&
          <Table
            rows={this.sortRowsByDate(reading.list)}
            setDetailRoute={this.getDetailRoute}
            columns={readingTable}/>
        }
      </div>
    )
  }
}

export default connect(
  (state) => ({
    device: state.device,
    mobileDevice: state.mobileDevice,
    reading: state.reading,
    review: state.review,
    user: state.user
  })
)(UserDetail)
