import React, { Component } from 'react';
import { connect } from 'react-redux';
import amazonStorage from '../../Storage/amazonStorage';
import userActions from '../../Actions/user'; 
import Table from '../../Components/Table';
import Search from '../../Components/Search';
import usersTable from '../../Tables/user';


class UserList extends Component {
  constructor() {
    super();
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
    this.handleSearchTypeClick = this.handleSearchTypeClick.bind(this);
    this.handleClearClick = this.handleClearClick.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(userActions.list())
    this.setState({
      searchFields: {
        email: "Email",
        first_name: "First Name",
        last_name: "Last Name"
      },
      selectedSearchField: 'email'
    })
  }

  handleClearClick() {
    this.props.dispatch(userActions.list())
  }

  getDetailRoute(column) {
    return `/users/${column.id}/`;
  }

  handleSearchSubmit(searchValue) {
    this.props.dispatch(userActions.list({ [this.state.selectedSearchField]: searchValue }))
  }

  handleSearchTypeClick(fieldKey) {
    this.setState({
      selectedSearchField: fieldKey
    })
  }

  render() {
    const { searchFields, selectedSearchField } = this.state;
    return (
      <div className="nm-layout">
        <h1>Nima Partners Admin</h1>
        <Search onSearchSubmit={this.handleSearchSubmit} placeholder="Search Customers"/>
        <div className="nm-spacer-12"/>
        <div className="nm-layout">
          {
            Object.keys(searchFields).map((key, index) => {
              let selectedStyle = key === selectedSearchField ? "selected" : "";
              return (
                <p
                  key={index}
                  onClick={() => { this.handleSearchTypeClick(key) }}
                  className={`nm-search__type ${selectedStyle}`}>
                  {searchFields[key]}
                </p>
              )
            })
          }
          <p
            onClick={this.handleClearClick}
            className="nm-search__type selected nm-margin-clear nm-pull--right">
              Clear
          </p>
        </div>
        <div className="nm-spacer-48"/>
        {
          this.props.user.list.Items &&
          <Table
            setDetailRoute={this.getDetailRoute}
            rows={this.props.user.list.Items}
            columns={usersTable}/>
        }
        <div className="nm-spacer-48"/>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    auth: state.auth,
    user: state.user,
    review: state.review
  })
)(UserList)
