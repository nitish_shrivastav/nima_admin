import React, { Component } from 'react';
import { connect } from 'react-redux';
import subscriptionActions from '../../Actions/subscription'; 
import Table from '../../Components/Table';
import Search from '../../Components/Search';
import shopifyUsersTable from '../../Tables/shopify_users';

class SubscriptionList extends Component {
  constructor() {
    super();
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
  }

  getDetailRoute(column) {
    return `/subscriptions/${column.id}/`;
  }
  
  handleSearchSubmit(searchValue) {
    this.props.dispatch(subscriptionActions.list({ search: searchValue }))
  }

  render() {
    return (
      <div>
        <div className="nm-layout">
          <h1>Nima Partners  Subscriptions</h1>
          <Search onSearchSubmit={this.handleSearchSubmit} placeholder="Search Subscriptions"/>
          <div className="nm-spacer-12"/>
          <div className="nm-spacer-48"/>
          <div className="nm-spacer-48"/>
          {
            this.props.subscription.list.customers &&
            <Table
              setDetailRoute={this.getDetailRoute}
              rows={this.props.subscription.list.customers}
              columns={shopifyUsersTable}/>
          }
          {
            !this.props.subscription.list.customers &&
            <p className="">
              Please search to find shopify customers
            </p>
          }
          <div className="nm-spacer-48"/>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    subscription: state.subscription
  })
)(SubscriptionList)
