import React, { Component } from 'react';
import moment from 'moment';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Table from '../../Components/Table';
import LabeledRow from '../../Components/LabeledRow';
import Modal from '../../Components/Modal';
import SubscriptionForm from '../../Components/Forms/SubscriptionForm';
import subscriptionActions from '../../Actions/subscription'; 
import subscriptionTable from '../../Tables/subscription';
import formActions from '../../Actions/form'; 
import timeHelper from '../../Helpers/timeHelper';
import variantHelper from '../../Helpers/variantHelper';
import config from '../../config';


class SubscriptionDetail extends Component {
  constructor() {
    super();
    this.getOrderLink = this.getOrderLink.bind(this);
    this.handleRowClick = this.handleRowClick.bind(this);
    this.getDiscounts = this.getDiscounts.bind(this);
    this.getSkips = this.getSkips.bind(this);
    this.getCanceledAt = this.getCanceledAt.bind(this);
    this.handleCloseClick = this.handleCloseClick.bind(this);
    this.getVariantName = this.getVariantName.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.submitDelete = this.submitDelete.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.submitEdit = this.submitEdit.bind(this);
    this.handleModalClick = this.handleModalClick.bind(this);
  }

  componentWillMount() {
    const { dispatch, match } = this.props;
    dispatch(subscriptionActions.retrieve(match.params.shopify_user_id));
    this.setState({
      showModal: false
    })
  }

  getOrderLink(row) {
    let order_id = row.order.split('|')[0];
    return `${config.shopify_domain}/admin/orders/${order_id}`
  }

  handleRowClick(option) {
    this.props.dispatch(subscriptionActions.selectSubscription(option))
  }

  getSkips() {
    let skips = this.props.subscription.selectedSubscription.skips;
    if (skips && skips.length > 0) {
      return skips.map((skip) => {
        return timeHelper.formatMomentDateWithoutTimezone(parseInt(skip.created_at));
      }).join('\n')
    }
    else {
      return "XXXXX";
    }
  }

  /** prettifySendDay() - convert shipping interval information into readable text
   * @prop {number as string} frequency - i.e. '1'
   * @prop {string} send_day - i.e. 'months|19'
   * @returns {string} a readable string, i.e. 'Send every 1 month on the 19th'
   */
  prettifySendDay() {
    const { shipping_interval_frequency: frequency, send_day } = this.props.subscription.selectedSubscription;
    let frequencyUnit = send_day.split('|')[0];
    frequencyUnit = frequencyUnit.slice(0, frequencyUnit.length - 1);
    const sendDay = send_day.split('|')[1];
    const dayOfTheMonth = timeHelper.dayOfMonth(sendDay);
    return `Send every ${frequency} ${frequencyUnit}${frequency > 1 ? "s" : ""} on the ${dayOfTheMonth}`
  }

  getDiscounts() {
    if (this.props.subscription.selectedSubscription.discounts) {
      return this.props.subscription.selectedSubscription.discounts.join('\n');
    }
    else {
      return "XXXXX";
    }
  }

  getCanceledAt() {
    if (this.props.subscription.selectedSubscription.deleted_at) {
      return timeHelper.formatMomentWithoutTimezone(this.props.subscription.selectedSubscription.deleted_at);
    }
    else {
      return "XXXXX";
    }
  }

  handleCloseClick() {
    this.props.dispatch(formActions.toggleModal(false))
    this.setState({ showModal: false })
  }

  getVariantName() {
    if (this.props.subscription.selectedSubscription) {
      return variantHelper.getVariantName(this.props.subscription.selectedSubscription.order.split('|')[1])
    }
    else {
      return "XXXXX";
    }
  }

  handleEditClick() {
    this.props.dispatch(formActions.toggleModal(true))
  }

  submitEdit(body) {
    this.props.dispatch(subscriptionActions.update(body))
  }

  handleDeleteClick() {
    this.setState({ showModal: true })
  }

  submitDelete() {
    let body = this.props.subscription.selectedSubscription;
    this.props.dispatch(subscriptionActions.destroy(body))
    this.setState({ showModal: false })
  }

  sortByDate(subscriptions) {
    return _.sortBy(subscriptions, ['created_at']).reverse()
  }

  handleModalClick() {
    this.setState({
      showModal: false
    })
  }

  render() {
    const { subscription } = this.props;
    
    return (
      <div>
      {
        subscription.detail &&
        <div className="nm-layout">
          <div>
            <h1 className="nm-layout">
              {subscription.detail.customer.email}
            </h1>
            <h2 className="nm-layout">
              {subscription.detail.customer.first_name} {subscription.detail.customer.last_name}
            </h2>
            <div className="nm-spacer-12"/>
            <div>
              <h4>Address:</h4>
              <p>{subscription.detail.customer.default_address.address1}</p>
              <p>{subscription.detail.customer.default_address.address2}</p>
              <p>{subscription.detail.customer.default_address.city}, 
              {subscription.detail.customer.default_address.province_code}</p>
              <p>{subscription.detail.customer.default_address.zip}</p>
              <p>{subscription.detail.customer.default_address.country}</p>
            </div>
            <div className="nm-spacer-48"/>
            <div className="nm-layout">
              {
                subscription.selectedSubscription &&
                <div className="nm-layout nm-layout__column--80">
                  <LabeledRow
                    column1Label="Item"
                    column1Text={this.getVariantName()}
                    column2Label="Send Day"
                    column2Text={this.prettifySendDay()}
                  />
                  <LabeledRow
                    column1Label="Payment provider"
                    column1Text={subscription.selectedSubscription.payment_provider}
                    column2Label="Payment Customer id"
                    column2Text={subscription.selectedSubscription.payment_customer_id}/>
                  <LabeledRow
                    column1Label="Shopify Customer Id"
                    column1Text={subscription.selectedSubscription.id}
                    column2Label="Last Sent"
                    column2Text={timeHelper.formatMomentWithoutTimezone(subscription.selectedSubscription.last_sent)}/>
                  <LabeledRow
                    column1Label="Canceled At"
                    column1Text={this.getCanceledAt()}
                    column2Label="Created At"
                    column2Text={timeHelper.formatMomentWithoutTimezone(subscription.selectedSubscription.created_at)}/>
                  <LabeledRow
                    column1Label="Upcoming Skips"
                    column1Text={this.getSkips()}
                    column2Label="Applied Discounts"
                    column2Text={this.getDiscounts()}/>
                  <LabeledRow
                    column1Label="Quantity"
                    column1Text={subscription.selectedSubscription.quantity}
                  />
                  {
                    subscription.selectedSubscription.constant_price &&
                    <LabeledRow
                      column1Label="Constant Price"
                      column1Text={`$${subscription.selectedSubscription.constant_price}`} />
                  }
                  <div className="nm-spacer-48"/>
                  <div className="nm-spacer-48"/>
                  {
                    !subscription.selectedSubscription.deleted_at &&
                    <div>
                      <h3 className="nm-layout nm-layout__column nm-link" onClick={this.handleEditClick}>
                        Edit Subscription
                      </h3>
                      <h3 className="nm-layout nm-layout__column nm-pointer nm-basic__red" onClick={this.handleDeleteClick}>
                        Cancel Subscription
                      </h3>
                    </div>
                  }
                </div>
              }
            </div>
            <div className="nm-spacer-48"/>
            <Table
              setDetailRoute={this.getOrderLink}
              onRowClick={this.handleRowClick}
              isExternalLink={true}
              rows={this.sortByDate(subscription.detail.Items)}
              columns={subscriptionTable}/>
            <Modal
              showModal={this.state.showModal}>
              <div className="nm-layout">
                <h1 className="nm-layout">
                  Cancel Subscription?
                </h1>
                <p className="">
                  Are you sure you want to cancel {this.getVariantName()} for
                  {subscription.detail.customer.first_name} {subscription.detail.customer.last_name}
                </p>
                <div className="nm-spacer-48"/>
                <p
                  className="nm-layout nm-layout__column nm-text-center nm-pointer nm-basic__red"
                  onClick={this.submitDelete}>
                  Cancel Subscription
                </p>
                <p
                  className="nm-layout nm-layout__column nm-link nm-text-center"
                  onClick={this.handleModalClick}>
                  Exit
                </p>
              </div>
            </Modal>
            <Modal
              showModal={this.props.form.modalIsOpen}
              className="nm-card__form"
              onCloseClick={this.handleCloseClick}>
              <SubscriptionForm 
                methodText="Update"
                initialFields={subscription.selectedSubscription}
                onSubmit={this.submitEdit} />
            </Modal>
            <div className="nm-spacer-48"/>
            <div className="nm-spacer-48"/>
          </div>
        </div>
      }
      </div>
    )
  }
}

export default connect(
  (state) => ({
    subscription: state.subscription,
    form: state.nmform
  })
)(SubscriptionDetail)
