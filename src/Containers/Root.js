import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Loader from '../Components/Loader';
import amazonStorage from '../Storage/amazonStorage';
import messageActions from '../Actions/message';

class Root extends Component {
  constructor() {
    super();
    this.handleErrorClick = this.handleErrorClick.bind(this);
  }

  handleErrorClick() {
    this.props.dispatch(messageActions.clearAllMessages());
  }

  render() {
    return (
      <div className="nm-layout">
        {
          this.props.loader.isLoading !== 0 &&
          <Loader />
        }
        {
         this.props.message.errorMessage && 
         <p onClick={this.handleErrorClick}>{this.props.message.errorMessage}</p>
        }
        <div className="nm-layout__center">
          {this.props.children}
          <div className="nm-clear"></div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    auth: state.auth,
    message: state.message,
    loader: state.loader
  })
)(Root)
