import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux'
import authActions from '../Actions/auth'

class Login extends Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    this.setState({
      email: "",
      password: "",
      loginAttempt: false 
    })
  }

  handleChange(e, field) {
    this.setState({
      [field]: e.target.value
    })
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.dispatch(authActions.login({ email: this.state.email, password: this.state.password }))
    this.setState({
      loginAttempt: true
    })
  }

  render() {
    const { auth, location } = this.props;
    if (this.state.loginAttempt === true && auth.user && location.state && location.state.from) {
      return ( <Redirect to={`${location.state.from.pathname}${location.state.from.search}`}/> )
    } else if (this.state.loginAttempt === true && auth.user) {
      return ( <Redirect to="/"/> )
    } else {
      return (
        <div>
          <div className="nm-spacer-48"></div>
          <div className="nm-spacer-48"></div>
          <h1>Login to Nima Partners admin</h1>
          <div className="nm-spacer-12"></div>
          <div className="nm-spacer-12"></div>
          <form onSubmit={this.handleSubmit}>
            <input onChange={(e) => { this.handleChange(e, 'email') }} placeholder="Email"/>
            <div className="nm-spacer-12"></div>
            <div className="nm-clear"></div>
            <input onChange={(e) => { this.handleChange(e, 'password') }} type="password" placeholder="Password"/>
            <div className="nm-spacer-12"></div>
            <div className="nm-clear"></div>
            <button type="submit">
              Login
            </button>
          </form>
          <div className="nm-spacer-12"></div>
          <p className="">
            Problems? Contact Paul or Bryant
          </p>
        </div>
      )
    }
  }
}

export default connect(
  (state) => ({
    auth: state.auth
  })
)(Login)