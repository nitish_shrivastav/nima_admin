import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Search from '../../Components/Search';


class VoucherList extends Component {
  constructor() {
    super();
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
  }

   handleSearchSubmit(searchValue) {
    this.props.history.push(`/vouchers/${searchValue}`)
  }

  render() {
    return (
      <div>
        <div className="nm-layout">
          <h1>Nima Partners  Vouchers</h1>
          <Search onSearchSubmit={this.handleSearchSubmit} placeholder="Search Vouchers"/>
          <div className="nm-spacer-12"/>
          <div className="nm-layout">
            <p
              onClick={this.handleClearClick}
              className="nm-search__type selected nm-margin-clear nm-pull--right">
                Clear
            </p>
          </div>
          <div className="nm-spacer-48"/>
          <h3 className="">Create New Vouchers</h3>
          <Link className="nm-link" to="/vouchers/bulk_create">Bulk Create</Link>
          <div className="nm-spacer-48"/>
          <div className="nm-spacer-48"/>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    voucher: state.voucher
  })
)(VoucherList);
