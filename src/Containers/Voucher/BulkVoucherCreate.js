import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import _ from 'lodash';

import BulkVoucherForm from '../../Components/Forms/BulkVoucherForm';
import Table from '../../Components/Table';
import timeHelpers from '../../Helpers/timeHelper';
import VoucherActions from '../../Actions/voucher';
import voucherTable from '../../Tables/voucher';



class BulkVoucherCreate extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  getDetailRoute(column) {
    return `/vouchers/${column.id}/`;
  }

  handleSubmit(values) {
    const body = values;
    body.create_s3_csv = true;
    this.props.dispatch(VoucherActions.bulkCreate(body));
  }

  render() {
    return (
      <div className="nm-layout">
        <div className="nm-layout">
          <Link to="/vouchers" className="nm-link nm-pull--left">
            &larr; Back to all vouchers
          </Link>
          <div className="nm-spacer-12"></div>
          <div className="nm-spacer-12"></div>
          {
            !_.get(this, 'props.voucher.bulkList') &&
            <BulkVoucherForm onSubmit={this.handleSubmit} />
          }
          {
            _.get(this, 'props.voucher.bulkList.data.s3_file') &&
            <div className="nm-layout">
              <a
                className="nm-layout nm-link"
                href={_.get(this, 'props.voucher.bulkList.data.s3_file.src')}
                download
              > Download CSV discounts
              </a>
              <p className="nm-layout">
                Expires at {timeHelpers.formatMomentWithoutTimezone(_.get(this, 'props.voucher.bulkList.data.s3_file.expires_at'))}
              </p>
            </div>
          }
          {
            _.get(this, 'props.voucher.bulkList') &&
            <Table
              setDetailRoute={this.getDetailRoute}
              rows={_.get(this, 'props.voucher.bulkList.data.vouchers')}
              columns={voucherTable}
            />
          }
        </div>
      </div>
    )
  }

}

export default connect(
  (state) => ({
    voucher: state.voucher
  })
)(BulkVoucherCreate);
