import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import _ from 'lodash';

import VoucherForm from '../../Components/Forms/VoucherForm';
import formActions from '../../Actions/form';
import LabeledRow from '../../Components/LabeledRow';
import Modal from '../../Components/Modal';
import Search from '../../Components/Search';
import subscriptionActions from '../../Actions/subscription'; 
import timeHelper from '../../Helpers/timeHelper';
import variantHelpers from '../../Helpers/variantHelper';
import VoucherActions from '../../Actions/voucher';


class VoucherDetail extends Component {
  constructor() {
    super();
    this.state = { showDeleteModal: false };
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleCloseClick = this.handleCloseClick.bind(this);
    this.handleDeleteModalClick = this.handleDeleteModalClick.bind(this);
    this.submitDelete = this.submitDelete.bind(this);
    this.toggleDeleteModal = this.toggleDeleteModal.bind(this);
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(formActions.toggleModal(false));
    //NOTE: right now back end is case sensitive to accept only all upper case letters
    this.props.dispatch(VoucherActions.retrieve(this.props.match.params.voucher_id));
  }

  handleSearchSubmit(searchValue) {
    this.props.dispatch(VoucherActions.retrieve(searchValue));
    this.props.history.push(`/vouchers/${searchValue}`)
  }

  handleCloseClick() {
    this.props.dispatch(formActions.toggleModal(false))
  }

  handleEditClick() {
    this.props.dispatch(formActions.toggleModal(true));
  }

  handleUpdate(values) {
    let line_items = Object.keys(values.line_items).map((key) => {
      if (values.line_items[key]) {
        return { 
          variant_id: key.split('_')[1],
          quantity: 1
        };
      }
      return undefined
    }).filter(item => item);
    let body = {
      ...values,
      line_items
    }
    let id = this.props.voucher.detail.id;
    this.props.dispatch(VoucherActions.update(id, body));
    this.props.dispatch(formActions.toggleModal(false));
    this.props.dispatch(VoucherActions.retrieve(this.props.match.params.voucher_id));
  }

  toggleDeleteModal() {
    this.setState({ showDeleteModal: !this.state.showDeleteModal })
  }

  handleDeleteModalClick() {
    this.toggleDeleteModal();
  }

  submitDelete() {
    let body = this.props.voucher.detail;
    this.props.dispatch(VoucherActions.delete(body))
    this.toggleDeleteModal();
    this.setState({ showDeleteSuccessModal: true });
  }

  //TODO: there are multiple modals, therefore, make a store.modal, and store your modals there

  render() {
    const { voucher } = this.props;
    return (
      <div>
        <Modal showModal={this.state.showDeleteModal}>
          <div className="nm-layout">
              <h1 className="nm-layout">Delete Voucher?</h1>
              <p className="">
                Are you sure you want to delete voucher # {_.get(voucher, 'detail.id')}?
              </p>
              <div className="nm-spacer-48"/>
              <p
                className="nm-layout nm-layout__column nm-text-center nm-pointer nm-basic__red"
                onClick={this.submitDelete}>
                Delete Voucher
              </p>
              <p
                className="nm-layout nm-layout__column nm-link nm-text-center"
                onClick={this.handleDeleteModalClick}>
                Exit
              </p>
            </div>
        </Modal>
        <Modal showModal={this.state.showDeleteSuccessModal}>
          <div className="nm-layout">
            <p>Voucher has been deleted</p>
            <Link to={'/vouchers'} className="nm-link">
              Return to Vouchers
            </Link>
          </div>å
        </Modal>
        {
          _.get(voucher, 'detail.id') &&
          <React.Fragment>
            <h1 className="nm-layout">
              Voucher: {voucher.detail.id}
            </h1>
            <Search onSearchSubmit={this.handleSearchSubmit} placeholder="Search Vouchers"/>
            <div className="nm-spacer-48"/>
            <h3 className="nm-layout nm-layout__column nm-link" onClick={this.handleEditClick}>
              Edit Voucher
            </h3>
            <h3 className="nm-layout nm-layout__column nm-pointer nm-basic__red" onClick={(this.handleDeleteModalClick)}>
              Delete Voucher
            </h3>
            <div className="nm-spacer-48"/>
            <div className="nm-layout">
              <div className="nm-layout nm-layout__column--80">
              <LabeledRow
                  column1Label="Started At"
                  column1Text={timeHelper.formatMomentWithTimezone(voucher.detail.start_date)}
                  column2Label="Ended At"
                  column2Text={timeHelper.formatMomentWithTimezone(voucher.detail.end_date)}/>
                <div className="nm-spacer-48"/>
              </div>
              <div className="nm-layout nm-layout__column--80" style={{whiteSpace: 'pre-wrap'}}>
                <LabeledRow
                  column1Label="Last Updated"
                  column1Text={timeHelper.formatMomentWithTimezone(voucher.detail.updated_at)}
                  column2Label="Created At"
                  column2Text={timeHelper.formatMomentWithTimezone(voucher.detail.created_at)} />
                <div className="nm-spacer-48"/>
              </div>
            </div>
          </React.Fragment>
        }
        {
          this.props.form.modalIsOpen &&
          voucher.detail &&
          <Modal
            showModal={this.props.form.modalIsOpen}
            className="nm-card__form"
            onCloseClick={this.handleCloseClick}>
            <VoucherForm
              methodText="Update"
              initialFields={voucher.detail}
              onSubmit={this.handleUpdate} />
          </Modal>
        }
      </div>
    )
  }
}

export default connect(
  (state) => ({
    voucher: state.voucher,
    form: state.nmform
  })
)(VoucherDetail);
