import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import amazonStorage from '../Storage/amazonStorage';
import authActions from '../Actions/auth';

class AuthenticatedRoute extends Component {
  constructor() {
    super();
    this.handleLogoutClick = this.handleLogoutClick.bind(this);
  }

  componentWillMount() {
    this.setState({
      logoutClicked: false 
    })
  }

  handleLogoutClick() {
    amazonStorage.clearAmazonUser();
    this.props.dispatch(authActions.logout());
    this.setState({
      logoutClicked: true
    })
  }

  render() {
    if (this.state.logoutClicked) return ( <Redirect to="/login" /> );
    else {
      return (
        <div className="nm-layout">
          <div className="nm-spacer-48"/>
          <p className="nm-link nm-pull--right" onClick={this.handleLogoutClick}>
            Logout
          </p>
          <Link className="nm-link nm-pull--right" to="/vouchers" style={{marginRight: 10}}>
            Vouchers
          </Link>
          <Link className="nm-link nm-pull--right" to="/" style={{marginRight: 10}}>
            Users
          </Link>
          <Link className="nm-link nm-pull--right" to="/discounts" style={{marginRight: 10}}>
            Discounts
          </Link>
          <Link className="nm-link nm-pull--right" to="/subscriptions" style={{marginRight: 10}}>
            Subscriptions
          </Link>
          { this.props.children }
        </div>
      )
    }
  }
}

export default connect(
  (state) => ({
  })
)(AuthenticatedRoute)
