import React, { Component } from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import LabeledRow from '../../Components/LabeledRow';
import userActions from '../../Actions/user';
import readingActions from '../../Actions/reading';
import reviewActions from '../../Actions/review';
import timeHelper from '../../Helpers/timeHelper';
import readingHelper from '../../Helpers/readingHelper';


class ReadingDetail extends Component {
  constructor() {
    super();
    this.handleJohnClick = this.handleJohnClick.bind(this);
    this.getProviderText = this.getProviderText.bind(this);
    this.getReviewRating = this.getReviewRating.bind(this);
  }

  componentWillMount() {
    const { dispatch, match, user, reading } = this.props;
    dispatch(readingActions.clearAll());
    dispatch(userActions.retrieve(match.params.user_id));
    dispatch(readingActions.retrieve(match.params.reading_id, match.params.user_id));
    this.setState({
      backUrl: `/users/${match.params.user_id}`,
      johnText: "Download test log →"
    })
  }

  handleJohnClick() {
    this.setState({
      johnText: "Not yet John... Not yet.."
    })
  }

  getProviderText() {
    const { review } = this.props;
    let provider = "Unknown";
    switch (review.detail.type_) {
      case "restaurant_dish":
        provider = review.detail.place_name;
        break;
      case "packaged_food":
        provider = review.detail.brand_name;
        break;
      default:
        break;
    }
    return `${review.detail.type_} | ${provider}`;
  }

  getAllTestsInReview() {
    const { review } = this.props;
    return review.detail.readings.map((reading) => {
      return timeHelper.formatMomentWithTimezone(reading.local_time)
    }).join("<br/>")
  }

  getReviewRating() {
    const { review } = this.props;
    let rating = "N/A";
    switch (review.detail.type_) {
      case "restaurant_dish":
        rating = `acc: ${parseFloat(review.detail.accommodation_score).toFixed(1)} | ava: ${parseFloat(review.detail.availability_score).toFixed(1)}`
        break;
      case "packaged_food":
        rating = review.detail.recommended_score ? "Recommended" : "Not Recommended"
        break;
      default:
        break;
    }
    return rating;
  }

  render() {
    const { reading, review, user } = this.props;
    return (
      <div>
      {
        user.detail &&
        <div>
          <div className="nm-layout">
            <Link to={this.state.backUrl} className="nm-link nm-pull--left">
              &larr; Back to {user.detail.first_name} {user.detail.last_name}
            </Link>
          </div>
          {
            reading.detail &&
            <div>
              <h1 className="nm-layout">
                Test { timeHelper.formatMomentWithTimezone(reading.detail.local_time) }
              </h1>
              <div className="nm-spacer-48"/>
              <div className="nm-layout">
                <div className="nm-layout nm-layout__column--80">
                  <LabeledRow
                    column1Label="RESULT"
                    column1Text={readingHelper.mapReadingToTextResult(reading.detail.test_result, reading.detail.allergen)}
                    column2Label="DETAIL"
                    column2Text={readingHelper.mapReadingDetailToText(reading.detail.result_detail).status}/>
                  <LabeledRow
                    column1Label="RESULT TEXT"
                    column1Text={readingHelper.mapReadingDetailToText(reading.detail.result_detail).description}/>
                  {
                    reading.detail.test_status &&
                    <LabeledRow
                      column1Label="Status"
                      column1Text={reading.detail.test_status.status}
                      column2Label="Descriptions"
                      column2Text={reading.detail.test_status.description}/>
                  }
                  {
                    reading.detail.deleted_at &&
                    <LabeledRow
                      column1Label="DELETED AT"
                      column1Text={timeHelper.formatMomentWithTimezone(reading.detail.deleted_at)} />
                  }
                  <div className="nm-spacer-48"/>
                  <div className="nm-spacer-48"/>
                </div>
                {
                  review.detail && review.selectedReading &&
                  <div className="nm-layout nm-layout__column--80">
                    <LabeledRow
                      column1Label="FOOD PROVIDER"
                      column1Text={this.getProviderText()}
                      column2Label="FOOD ITEM"
                      column2Text={review.selectedReading.item_name}/>
                    <LabeledRow
                      column1Label="SPECIFIED ALLERGEN FREE"
                      column1Text={review.selectedReading.labeling}
                      column2Label="RATING/RECOMMENDATION"
                      column2Text={this.getReviewRating()}/>
                    <LabeledRow
                      column1Label="COMMENT"
                      column1Text={review.detail.notes} />
                    <LabeledRow
                      column1Label="TESTS IN THIS REVIEW"
                      column1Text={this.getAllTestsInReview()} />
                  </div>
                }
              </div>
              <div className="nm-spacer-48"/>
            </div>
          }
        </div>
      }
      </div>
    )
  }
}

export default connect(
  (state) => ({
    user: state.user,
    reading: state.reading,
    review: state.review
  })
)(ReadingDetail)
