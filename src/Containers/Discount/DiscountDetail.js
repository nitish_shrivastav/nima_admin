import React, { Component } from 'react';
import config from '../../config';
import moment from 'moment';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import LabeledRow from '../../Components/LabeledRow';
import Table from '../../Components/Table';
import discountActions from '../../Actions/discount'; 
import formActions from '../../Actions/form'; 
import timeHelper from '../../Helpers/timeHelper';
import discountUsesTable from '../../Tables/discountUses';
import DiscountForm from '../../Components/Forms/DiscountForm';
import Modal from '../../Components/Modal';
import variantHelpers from '../../Helpers/variantHelper';


class DiscountDetail extends Component {
  constructor() {
    super();
    this.getDiscountAmount = this.getDiscountAmount.bind(this);
    this.getRuleText = this.getRuleText.bind(this);
    this.getRuleLabel = this.getRuleLabel.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCloseClick = this.handleCloseClick.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.convertVariantIds = this.convertVariantIds.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(formActions.toggleModal(false))
    this.props.dispatch(discountActions.retrieve(this.props.match.params.discount_id.toLowerCase()));
  }

  getDiscountAmount() {
    if (this.props.discount.detail.type_ === "percent") {
      return `${this.props.discount.detail.amount}%`
    }
    else {
      return `$${this.props.discount.detail.amount}`
    }
  }

  getRuleText(key) {
    if (key === "min_amount") {
      return `$${this.props.discount.detail.rules[key]}`
    }
    else {
      return this.props.discount.detail.rules[key]
    }
  }

  getRuleLabel(key) {
    return key;
  }

  handleCloseClick() {
    this.props.dispatch(formActions.toggleModal(false))
  }

  handleSubmit(body) {
    this.props.dispatch(discountActions.update(body))
  }

  handleEditClick() {
    this.props.dispatch(formActions.toggleModal(true))
  }

  sortByDate(discounts) {
    return _.sortBy(discounts, ['created_at']).reverse();
  }

  /** convertVariantIds() - converts an array of variantIds into a comma delimited string of labels
   * @prop {array} group - array of variantIds, i.e. [700, 800, 900]
   * @returns {string} returns comma delimited string of labels, i.e. ['Gluten Capsule Pack - One 12-pack', 'Sensor Only - Peanut']
   */
  convertVariantIds() {
    return this.props.discount.detail.group.map(id => variantHelpers.getVariantName(id)).join(', \n');
  }

  render() {
    const { discount } = this.props;
    return (
      <div>
        <div className="nm-layout">
          <Link to="/discounts" className="nm-link nm-pull--left">
            &larr; Back to all discounts
          </Link>
        </div>
        {
          discount.detail &&
          <div>
            <h1 className="nm-layout">
              Discount: {discount.detail.id}
            </h1>
            <h3 className="nm-layout" onClick={this.handleEditClick}>Edit</h3>
            <div className="nm-spacer-48"/>
            <div className="nm-layout">
              <div className="nm-layout nm-layout__column--80">
                <LabeledRow
                  column1Label="Type"
                  column1Text={discount.detail.type_}
                  column2Label="Use Count"
                  column2Text={discount.detail.use_count}/>
                <div className="nm-spacer-48"/>
              </div>
              <div className="nm-layout nm-layout__column--80" style={{whiteSpace: 'pre-wrap'}}>
                <LabeledRow
                  column1Label="Group"
                  column1Text={this.convertVariantIds()}
                  column2Label="Created At"
                  column2Text={timeHelper.formatMomentWithTimezone(discount.detail.created_at)} />
                <div className="nm-spacer-48"/>
                <LabeledRow
                  column1Label="Last Updated"
                  column1Text={timeHelper.formatMomentWithTimezone(discount.detail.updated_at)} />
                <h3>Rules</h3>
                {
                  Object.keys(discount.detail.rules).map((key) => {
                    return (
                      <LabeledRow
                        column1Label={this.getRuleLabel(key)}
                        column1Text={this.getRuleText(key)} />
                    )
                  })
                }
                <div className="nm-spacer-48"/>
              </div>
            </div>
            <Table
              rows={this.sortByDate(discount.detail.uses.Items)}
              setDetailRoute={this.setShopifyUrl}
              isExternalLink={true}
              columns={discountUsesTable}
              alternativeColumnHeaderForDetails="ORDER DETAILS"/>
            {
              this.props.form.modalIsOpen &&
              <Modal
                showModal={this.props.form.modalIsOpen}
                className="nm-card__form"
                onCloseClick={this.handleCloseClick}>
                <DiscountForm
                  methodText="Update"
                  initialFields={discount.detail}
                  onSubmit={this.handleSubmit} />
              </Modal>
            }
          </div>
        }
      </div>
    )
  }
}

export default connect(
  (state) => ({
    discount: state.discount,
    form: state.nmform
  })
)(DiscountDetail)
