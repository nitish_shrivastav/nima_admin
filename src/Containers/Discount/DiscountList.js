import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import { connect } from 'react-redux';
import discountActions from '../../Actions/discount'; 
import formActions from '../../Actions/form'; 
import Table from '../../Components/Table';
import Search from '../../Components/Search';
import discountsTable from '../../Tables/discount';
import DiscountForm from '../../Components/Forms/DiscountForm';
import Modal from '../../Components/Modal';

class DiscountList extends Component {
  constructor() {
    super();
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
    this.handleClearClick = this.handleClearClick.bind(this);
    this.openDisountCreate = this.openDisountCreate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCloseClick = this.handleCloseClick.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(discountActions.list())
    this.props.dispatch(formActions.toggleModal(false))
  }

  handleClearClick() {
    this.props.dispatch(discountActions.list())
  }

  getDetailRoute(column) {
    return `/discounts/${column.id}/`;
  }

  handleSearchSubmit(searchValue) {
    this.props.dispatch(discountActions.list({ id: searchValue }))
  }

  openDisountCreate() {
    this.props.dispatch(formActions.toggleModal(true))
  }

  handleCloseClick() {
    this.props.dispatch(formActions.toggleModal(false))
  }

  handleSubmit(body) {
    this.props.dispatch(discountActions.create(body))
  }

  sortItems(items) {
   return _.sortBy(items, ['started_at']).reverse()
  }

  render() {
    return (
      <div>
        <div className="nm-layout">
          <h1>Nima Partners Discounts</h1>
          <Search onSearchSubmit={this.handleSearchSubmit} placeholder="Search Discounts"/>
          <div className="nm-spacer-12"/>
          <div className="nm-layout">
            <p
              onClick={this.handleClearClick}
              className="nm-search__type selected nm-margin-clear nm-pull--right">
                Clear
            </p>
          </div>
          <div className="nm-spacer-48"/>
          <h3 className="" onClick={this.openDisountCreate}>Create New Discount</h3>
          <Link className="nm-link" to="/discounts/bulk_create">Bulk Create</Link>
          <div className="nm-spacer-48"/>
          {
            this.props.discount.list.Items &&
            <Table
              setDetailRoute={this.getDetailRoute}
              rows={this.sortItems(this.props.discount.list.Items)}
              columns={discountsTable}/>
          }
          <div className="nm-spacer-48"/>
        </div>
        {
          this.props.form.modalIsOpen &&
          <Modal
            showModal={this.props.form.modalIsOpen}
            className="nm-card__form"
            onCloseClick={this.handleCloseClick}>
            <DiscountForm
              methodText="Create"
              onSubmit={this.handleSubmit} />
          </Modal>
        }
      </div>
    )
  }
}

export default connect(
  (state) => ({
    discount: state.discount,
    form: state.nmform
  })
)(DiscountList)
