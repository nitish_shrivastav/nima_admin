import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import BulkDiscountForm from '../../Components/Forms/BulkDiscountForm';
import DiscountActions from '../../Actions/discount';
import discountsTable from '../../Tables/discount';
import Table from '../../Components/Table';
import timeHelpers from '../../Helpers/timeHelper';


class BulkDiscountCreate extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(values) {
    values.group = Object.keys(values.group).map((key) => {
      if (values.group[key]) {
        return {
          id: key
        }
      } else {
        return undefined;
      }
    }).filter(item => item)
    values.create_s3_csv = true;
    this.props.dispatch(DiscountActions.bulkCreate(values));
  }

  getDetailRoute(column) {
    return `/discounts/${column.id}/`;
  }

  render() {
    return (
      <div className="nm-layout">
        <div className="nm-layout">
          <Link to="/discounts" className="nm-link nm-pull--left">
            &larr; Back to all discounts
          </Link>
          <div className="nm-spacer-12"></div>
          <div className="nm-spacer-12"></div>
          {
            !this.props.discount.bulkList &&
            <BulkDiscountForm onSubmit={this.handleSubmit}/> 
          }
          {
            this.props.discount.bulkList &&
            this.props.discount.bulkList.data &&
            this.props.discount.bulkList.data.s3_file &&
            <div className="nm-layout">
              <a
                className="nm-layout nm-link"
                href={this.props.discount.bulkList.data.s3_file.src}
                download>
                Download CSV discounts
              </a>
              <p className="nm-layout">
                Expires at {timeHelpers.formatMomentWithTimezone(this.props.discount.bulkList.data.s3_file.expires_at)}
              </p>
            </div>
          }
          {
            this.props.discount.bulkList && this.props.discount.bulkList.data &&
            <Table
              setDetailRoute={this.getDetailRoute}
              rows={this.props.discount.bulkList.data.discounts}
              columns={discountsTable}/>
          }
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    discount: state.discount
  })
)(BulkDiscountCreate)
