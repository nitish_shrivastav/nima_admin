import React from 'react';
import { Route, Redirect, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import UserList from './User/UserList';
import Login from './Login';
import Root from './Root';
import UserDetail from './User/UserDetail';
import ReadingDetail from './Reading/ReadingDetail';
import DiscountList from './Discount/DiscountList';
import BulkDiscountCreate from './Discount/BulkDiscountCreate';
import SubscriptionList from './Subscription/SubscriptionList';
import SubscriptionDetail from './Subscription/SubscriptionDetail';
import DiscountDetail from './Discount/DiscountDetail';
import AuthenticatedRoute from './AuthenticatedRoute';
import amazonStorage from '../Storage/amazonStorage';
import messageActions from '../Actions/message';
import authActions from '../Actions/auth';
import VoucherDetail from './Voucher/VoucherDetail';
import VoucherList from './Voucher/VoucherList';
import BulkVoucherCreate from './Voucher/BulkVoucherCreate';


const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    amazonStorage.getAmazonUser() ? (
      <AuthenticatedRoute>
        <Component {...props}/>
      </AuthenticatedRoute>
    ) : (
      <Redirect to={{
        pathname: `/login`,
        state: { from: props.location }
      }}/>
    )
  )}/>
)

class App extends React.Component {

  componentDidUpdate(prevProps) {
    let locationDidMove = this.props.location !== prevProps.location;
    let amazonUser = amazonStorage.getAmazonUser()
    if (locationDidMove) {
      this.props.dispatch(messageActions.clearAllMessages());
    }
    if (locationDidMove && amazonUser) {
      let refreshInfo = {
        "email": amazonUser.email,
        "refresh_token": amazonUser.tokens.Refresh.Token
      }
      this.props.dispatch(authActions.refreshAuth(refreshInfo));
    }
  }

  render() {
    return (
      <div>
        <main>
          <Root>
            <Switch>
              <Route exact path="/login" component={Login} />
              <PrivateRoute exact path="/" component={UserList} />
              <PrivateRoute exact path="/discounts/bulk_create" component={BulkDiscountCreate} />
              <PrivateRoute exact path="/discounts/:discount_id" component={DiscountDetail} />
              <PrivateRoute exact path="/discounts" component={DiscountList} />
              <PrivateRoute exact path="/subscriptions" component={SubscriptionList} />
              <PrivateRoute exact path="/subscriptions/:shopify_user_id" component={SubscriptionDetail} />
              <PrivateRoute path="/users/:user_id/readings/:reading_id" component={ReadingDetail} />
              <PrivateRoute path="/users/:user_id" component={UserDetail} />
              <PrivateRoute exact path="/vouchers/bulk_create" component={BulkVoucherCreate} />
              <PrivateRoute exact path="/vouchers/:voucher_id" component={VoucherDetail} />
              <PrivateRoute path="/vouchers" component={VoucherList} />
            </Switch>
          </Root>
        </main>
      </div>
    )
  }
}


export default withRouter(connect(
  (state) => ({}))(App))
