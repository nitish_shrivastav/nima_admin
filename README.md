# [admin_portal] (https://github.com/nimasensor/admin_portal)

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). Create React App documentation goes into depth about everything React, see the documentation.

This app is run on production at `https://admin.nimasensor.com`
This app is run on staging at `https://stg-admin.nimasensor.com`

## Gettting Started

To get started make sure you have a current version of NPM and a current version of Nodejs.

In the root of the application run the command `npm install`
 
Once that command has finished, run `npm start` in the same directory and open your browser to `localhost:3000` to see the base of the app.

A knowledge of how to use `react-router` and `redux` is essential for the progress of this app.


## Purpose and Content

This app is used to doing admin level operations such as managing subscriptions, managing users, managing vouchers, and managing discounts.

## Architecture

This app is extremely similar to the [web_app] [https://github.com/nimasensor/web_app] (See the web_app's readme for more information).


## Config

There are 3 environments that this app can use:  `dev`, `stg`, and `production`. the commands `npm run build-stg` and `npm run build-prod` You can see the different configuration files by looking at `/src/config.prod.js`, `/src/config.stg.js` and `/src/config.dev.js`. Each file corresponds to the correct api layer. See each file for more details.

If you would like to use a different environment for development, specify which one you would like to use in the `/src/config.js` file. Don't forget to revert the logic for the environment back to its origional state.

## Deployments

Each deployment should itterate the version of the app. The version is displayed in the bottom right hand corner of the app. To iterate the version, look in the `/src/config.js` and there is a line that says `config.version = X.X.X`. Iterate that and run the build script again.

The deployment is set up to run to a s3 bucket which has a cloudfront distribution in from of the s3 bucket. All of this is taken care of including a cloudfront invalidation by running the `npm run build-stg && npm run deploy-stg`. Do deploy production you run `npm run build-prod && npm run deploy-prod`. This is a static application that pulls in data dynamically. There is no server side rendering for this app.